<?php
/**
 * Export view
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @version 	$Id: export.raw.php 2368 2013-03-08 14:17:15Z RolandD $
 */

defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controller');

class CsviControllerCsvi extends JControllerLegacy {

	/**
	* Overwrite the Joomla default getModel to make sure the ignore_request is not set to true
	*
	* @copyright
	* @author 		RolandD
	* @todo
	* @see
	* @access 		public
	* @param
	* @return
	* @since 		1.0
	*/
	public function getsef() {
		echo JRoute::_(base64_decode(JFactory::getApplication()->input->getBase64('parseurl')));
		
		JFactory::getApplication()->close();
	}
}