 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="tienda">
	<div class="maletas_botones">
		<div class="head_detail">
			<div class="product_name">
				<h1> Nombre de el producto </h1>
				<div class="ruta">
					<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
				</div>
			</div>
			<div class="pasos">
				<img src="images/dimension_estampados/paso-2.png">
			</div>

		</div>

		<form id="maleta_detail" action="<?php echo JRoute::_(''); ?>" method="post">


			<div class="content_mods">
				<div class="image">
					<img class="magic"src="asf">
				</div>

				<div class="c_cuarenta">
					<div class="cantidad">
						<h2 class="cant"> Escoje la cantidad </h2>
						<table class="cantidad_maletas" border="1">
							<tr>
								<td width="50%">CANTIDAD</td>
							</tr>
							<tr>
								<td><input id="cantidad_maletas" type="text"></td>
							</tr>						
						</table>

						<div class="content_price">
							<div class="calculadora">
								<img src="images/dimension_estampados/calculadora.png">
								<span class="calcular"></span>
							</div>
							<div class="precio_unitario" >
								<div class="title_precio">Precio Unitario</div>
								<input id="unitario_maletas" type="text" value="20000" DISABLED>
							</div>
							<div class="precio_unitario">
								<div class="title_precio">Precio Total</div>
								<input id="total_maletas" type="text" DISABLED>
							</div>
							<span class="azul">Precio IVA incluido</span>
						</div>
						<span class="gris">Ofrecemos descuentos para 5, 10, 20, 50,100, 200 y 500 unidades. <br>Compruebalo.Cambia la cantidad</span>
					
					</div>

					<div class="subir">
						<h2 class="sube">Sube tu diseño</h2>

		            	<input type="file" name="file" id="file" />

		            	<span class="azul">Al cargar esta imagen, certificas que tienes derecho a utilizarla</span>

		            	<span class="gris"> Haz click en "Examinar" para buscar en tu computador el archivo que deseas. Puedes cargar cualquier tipo de archivo.
		            		Peso maximo de archivo: 26 Mgb.</span>
					</div>
				</div>
			</div>
			<div class="links">
				<a href="#">Como Funciona</a>
				<a href="#">Info Producto</a>
				<a href="#">Info Tamaño Estampado</a>
			</div>
			<div class="agregar">
				<button id="agregar-producto">Agregar al carrito</button>
			</div>
			<div class="prob">
				<p>¿Problemas procesando tu compra? <br> Contactános y dejanos saber tu <br>inquietud. Tel:(571) 530 97 97 </p>
			</div>
		</form>	
	</div>
</div>


