 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="tienda">

	<div class="content_all_mugs">

		<div class="head_detail">
			<div class="product_name">
				<h1> Nombre de el producto </h1>
				<div class="ruta">
					<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
				</div>
			</div>
			<div class="pasos">
				<img src="images/dimension_estampados/paso-2.png">
			</div>

		</div>

		<form id="camisa_detail" action="<?php echo JRoute::_(''); ?>" method="post">

			<div class="clase_mugs">
				<h2 class="esc">Escojer Tipo de Mug</h2>
				<ul class="primer_nivel">
					<li class="tipos"> 
						<img src="images/tipo_mugs/MUG-IZQUIERDA.png">  <span class="tipo"> MUG IZQUIERDA </span>
					</li>
					<li class="tipos ">  
						<img src="images/tipo_mugs/MUG-DERECHA.png">  <span class="tipo"> MUG DERECHA </span>
					</li>
					<li class="tipos active"> 
						<span class="tipo"> RECOMENDADO </span>
						<img src="images/tipo_mugs/MUG-FRENTE.png">  <span class="tipo"> MUG FRENTE </span>
					</li>
					<li class="tipos">  
						<img src="images/tipo_mugs/MUG-A-DOS-CARAS.png">  <span class="tipo"> MUG A DOS CARAS </span>
					</li>
					<li class="tipos">  
						<img src="images/tipo_mugs/MUG-A-DOS-CARAS.png">  <span class="tipo"> MUG COMPLETO </span>
					</li>

				</ul>
			</div>

			<div class="content_mods">
				<div class="image">
					<img class="magic"src="asf">
				</div>

				<div class="c_cuarenta">
					<div class="cantidad">
						<div class="tamanio_imagen">
							<span class="title_tamanio"> Tamaño de a imagen:</span>
							<span class="alto">Alto <span class="valor_alto"> 8.5cm </span> </span>
							<span class="largo">Largo <span class="valor_largo">8.5cm</span> </span>
						</div>
						<h2 class="cant"> Escoje la cantidad de Mugs </h2>
						<table class="c_mugs" border="0">
							<tr>
								<td><input id="cantidad_mugs" type="text" value="0"></td>
							</tr>						
						</table>
<!-- 						<span class="azul">Información de las tallas</span> -->

						<div class="content_price">
							<div class="calculadora">
								<img src="images/dimension_estampados/calculadora.png">
								<span class="calcular"></span>
							</div>
							<div class="precio_unitario" >
								<div class="title_precio">Precio Unitario</div>
								<input id="unitario_mugs" type="text" value="20000" DISABLED>
							</div>
							<div class="precio_unitario">
								<div class="title_precio">Precio Total</div>
								<input id="total_mugs" type="text" DISABLED>
							</div>
							<span class="azul">Precio IVA incluido</span>
						</div>
						<span class="gris">Ofrecemos descuentos para 5, 10, 20, 50,100, 200 y 500 unidades. <br>Compruebalo.Cambia la cantidad</span>
					
					</div>

					<div class="subir">
						<h2 class="sube">Sube tu diseño</h2>

		            	<input type="file" name="file" id="file" />

		            	<span class="azul">Al cargar esta imagen, certificas que tienes derecho a utilizarla</span>

		            	<span class="gris"> Haz click en "Examinar" para buscar en tu computador el archivo que deseas. Puedes cargar cualquier tipo de archivo.
		            		Peso maximo de archivo: 26 Mgb.</span>
					</div>
				</div>
			</div>
			<div class="prob_mugs">
				<p>¿Problemas procesando tu compra? Contactános y dejanos saber tu inquietud. Tel:(571) 530 97 97 </p>
			</div>

			<div class="agregar">
				<button id="agregar-producto">Agregar al carrito</button>
			</div>

			<div class="guia">
				<a href="#"><img src="images/dimension_estampados/Guia-de-manejo.png"> <span class="gris">Guía de manejo <br> imagenes</span></a>
			</div>
		</form>	
	</div>
</div>


