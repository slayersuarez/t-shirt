 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="confirmar">

	<div class="head_detail">
		<div class="product_name">
			<h1> Mis Compras - Confirmar Pedido</h1>
			<div class="ruta">
				<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
			</div>
		</div>
		<div class="pasos">
			<img src="images/dimension_estampados/paso-2.png">
		</div>
		<div class="prob">
			<p>¿Problemas procesando tu compra?  Contactános y dejanos saber tu inquietud. Tel:(571) 530 97 97 </p>
		</div>
		<h4 class="subt"> Por favor confirma los datos de tu pedido</h4>
	</div>

	<form id="confirmar_pedido" action="<?php echo JRoute::_(''); ?>" method="post">
		<h4> Dirección de Facturación</h4>
			<table border="0" cellspacing="0" cellpadding="0" class="datos_envio">
				<tbody>
				<tr>
					<td colspan="2"> Daniel Andres Pinto</td>
				</tr>
				<tr>
					<td colspan="2">Carrera 11 No 92</td>
				</tr>
				<tr>
					<td colspan="2">Bogotá - Colombia</td>
				</tr>
				<tr>
					<td colspan="2">Tel: 530 97 97</td>
				</tr>
				<tr>
					<td colspan="2">Cel: 313 896 74 73</td>
				</tr>
				<tr>
					<td>E-mail: info@t-shirtlab.com</td>
					<td> <a href="#" class="cambiar"> Cambiar Dirección</a></td>
				</tr>
				</tbody>
			</table>

			<table border="0" cellspacing="1" cellpadding="1" class="list_products" width="100%">
			<tbody>
				<tr>
					<td class="titles_products"><span>Cantidad</span></td>
					<td class="titles_products"><span>Descripción de el Producto </span></td>
					<td class="titles_products"><span>Unidad </span></td>
					<td class="titles_products"><span>Total </span></td>
				</tr>
				<tr>
					<td class="all product_images"><span class="cantidad"> 10</span></td>
					<td class="all product_description"> 

						<span class="titulo"> Camiseta WOW negro cuello redondo </span>
						<span class="referencia"> No de Ref. 6666 </span>
						<span class="talla"> Talla</span>

					 </td>
					<td class="all product_price"> <span class="price"> $34.000</span> </td>
					<td class="all price_total"> <span class="sub_price"> $340.000 </span></td>
				</tr>
			</tbody>
			</table>

			<div class="seguro"><span class="azul"> Tu pedido Esta a salvo y seguro </span></div>

			<table border="0" class="totales">
				<tbody>
				<tr>
					<td>I.V.A 16%:</td>
					<td> <span class="iva">$56.000</span> </td>
				</tr>
				<tr>
					<td>Subtotal con I.V.A: </td>
					<td><span class="sub_total_iva">$396.000</span> </td>
				</tr>
				<tr>
					<td>Envió:</td>
					<td><span class="price_envio">$0</span></td>
				</tr>
				<tr>
					<td> TOTAL </td>
					<td><span class="total">$396.000</span> </td>
				</tr>
				</tbody>
			</table>

		<div class="opciones">

			<a  href="#" id="continuar">Continuar</a>

			<span>Haz Click para seleccionar tu forma de pago</span>

		</div>
	</form>	
</div>	