 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="tienda">

	<div class="head_detail">
		<div class="product_name">
			<h1> Nombre de el producto </h1>
			<div class="ruta">
				<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
			</div>
		</div>
		<div class="pasos">
			<img src="images/dimension_estampados/paso-2.png">
		</div>

	</div>

	<form id="camisa_detail" action="<?php echo JRoute::_(''); ?>" method="post">

		<div class="escojer">
			<h2 class="esc">Escoje el tamaño de el estampado</h2>
			<ul class="primer_nivel">
				<li class="bolsillo"> 
					<img src="images/dimension_estampados/bolsillo.png">  <span class="tipo"> BOLSILLO </span>
				</li>
				<li class="carta active">  
					<span class="tipo"> RECOMENDADO </span>
					<img src="images/dimension_estampados/carta-1.png">  <span class="tipo"> CARTA </span>
				</li>
				<li class="doble_carta">  
					<img src="images/dimension_estampados/doble-carta-1.png">  <span class="tipo"> DOBLE CARTA </span>
				</li>
				<li class="despliegue">
					<a href="#" id="ver-items">
						<img src="images/dimension_estampados/ver-mas.png">  <span class="tipo"> MAS OPCINES DE ESTAMPADOS </span>
					</a>
				</li>
				<li class="sub"> <img src="images/dimension_estampados/doble-carta-1.png"> <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doble-carta-2.png">  <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doble-carta-3.png">  <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doblecarta-carta.png"> <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doblecarta-doblecarta.png">  <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doble-carta-3.png">  <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doblecarta-carta.png"> <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doblecarta-doblecarta.png">  <span class="tipo"> Tipo de estampado </span></li>
				<li class="sub"> <img src="images/dimension_estampados/doble-carta-3.png">  <span class="tipo"> Tipo de estampado </span></li>
			</ul>
		</div>

		<div class="content_mods">
			<div class="image">
				<img class="magic"src="asf">

				<div class="infotallas">
					<div class="ic-cerrar"><a href="#"><img src="images/dimension_estampados/x.png"></a></div>
					<div class="imagen">
						<img src="images/dimension_estampados/dumi.png">
					</div>
					<table border="0" class="tallas">
						<tbody>
						<tr>
						    <td colspan="2" class="top">
								<h4>Tabla de Tallas Camiseta de Hombre</h4>
								<span class="white"> Toma la medida alrededor de el pecho y la espalda </span>
						    </td>
						</tr>
						<tr>
						   <td>S</td>
						   <td> 91cms - 97cms </td>
						</tr>
						<tr>
						   <td> M </td>
						   <td>98cms - 104cms</td>
						</tr>
						<tr>
						   <td>L</td>
						   <td>105cms - 112cms</td>
						</tr>
						<tr>
						   <td> Xl </td>
						   <td>113cms - 119cms</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="c_cuarenta">
				<div class="cantidad">
					<h2 class="cant"> Escoje la cantidad por talla </h2>
					<table class="tallas" border="1">
						<tr>
							<td>S</td>
							<td>M</td>
							<td>L</td>
							<td>XL</td>
							<td>TOTAL</td>
						</tr>
						<tr>
							<td><input id="s"  value="0" class="tallas-valores" type="text"></td>
							<td><input id="m"  value="0" class="tallas-valores" type="text"></td>
							<td><input id="l"  value="0" class="tallas-valores" type="text"></td>
							<td><input id="xl"  value="0" class="tallas-valores" type="text"></td>
							<td><input id="total_camisetas" type="text" value="0" DISABLED></td>
						</tr>						
					</table>
					<span class="azul">Información de las tallas</span>

					<div class="content_price">
						<div class="calculadora">
							<img src="images/dimension_estampados/calculadora.png">
							<span class="calcular"></span>
						</div>
						<div class="precio_unitario" >
							<div class="title_precio">Precio Unitario</div>
							<input id="unitario" type="text" value="20000" DISABLED>
						</div>
						<div class="precio_unitario">
							<div class="title_precio">Precio Total</div>
							<input id="result_suma" value="$0" type="text" DISABLED>
						</div>
						<span class="azul">Precio IVA incluido</span>
					</div>
					<span class="gris">Ofrecemos descuentos para 5, 10, 20, 50,100, 200 y 500 unidades. <br>Compruebalo.Cambia la cantidad</span>
				
				</div>

				<div class="subir">
					<h2 class="sube">Sube tu diseño</h2>

	            	<input type="file" name="file" id="file" />

	            	<span class="azul">Al cargar esta imagen, certificas que tienes derecho a utilizarla</span>

	            	<span class="gris"> Haz click en "Examinar" para buscar en tu computador el archivo que deseas. Puedes cargar cualquier tipo de archivo.
	            		Peso maximo de archivo: 26 Mgb.</span>
				</div>
			</div>
		</div>
		<div class="links">
			<a href="#">Como Funciona</a>
			<a href="#">Info Producto</a>
			<a href="#">Info Tamaño Estampado</a>
			<a class="info_tallas"href="#">Info Tallas</a>
		</div>
		<div class="agregar">
			<button id="agregar-producto">Agregar al carrito</button>
		</div>
		<div class="prob">
			<p>¿Problemas procesando tu compra? <br> Contactános y dejanos saber tu <br>inquietud. Tel:(571) 530 97 97 </p>
		</div>
	</form>	
</div>


