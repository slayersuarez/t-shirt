 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="envios">

	<div class="head_detail">
		<div class="product_name">
			<h1> Métodos de envió</h1>
			<div class="ruta">
				<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
			</div>
		</div>
		<div class="pasos">
			<img src="images/dimension_estampados/paso-2.png">
		</div>
		<div class="prob">
			<p>¿Problemas procesando tu compra?  Contactános y dejanos saber tu inquietud. Tel:(571) 530 97 97 </p>
		</div>
		<h4 class="subt"> Por favor confirmar el método de envió</h4>
	</div>

	<form id="metodo_envios" action="<?php echo JRoute::_(''); ?>" method="post">
		<table border="0" cellspacing="1" cellpadding="1" class="met_envios">
		<tbody>
			<tr>
			<td colspan="4" class="top"><h4>Selecciona la forma de envió</h4></td>
			</tr>
			<tr>
			<td widht="10">&nbsp;</td>
			<td widht="30"><h5>Transportista</h5></td>
			<td widht="30"><h5>Destino</h5></td>
			<td widht="30"><h5>Precio</h5></td>
			</tr>
			<tr>
			<td><input type="checkbox" name="transportadora" value="transportadora"></td>
			<td>Transportadora</td>
			<td>Bogotá - DC.</td>
			<td>Valor a cancelar contraentrega</td>
			</tr>
			<tr>
			<td><input type="checkbox" name="recogen" value="recogen"></td>
			<td>Recoger en T-Shirt (92)</td>
			<td> T-Shirt Lab Calle 92</td>
			<td>$0</td>
			</tr>
		</tbody>
		</table>
		<div class="desc">
			<h4>Tiempos de entrega por TRANSPOTADORA:</h4>
			<p>24 a 48 horas hábiles para ciudades principales.</p>
			<p> 48 a 72 horas hábiles a ciudades secundarias. </p>
			<p>Tiempos basados en ordenes recibidas antes de las 3:00 P.M. y cantidades inferiores a 100 unidades </p>
		</div>
		<div class="opciones">

			<a  href="#" id="continuar">Continuar</a>

			<span>Haz Click para seleccionar tu forma de pago</span>

		</div>
	</form>	
</div>	