 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>

<div id="gracias">

	<div class="head_detail">
		<div class="product_name">
			<h1> Gracias por su pedido</h1>
			<div class="ruta">
				<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
			</div>
		</div>
		<div class="pasos">
			<img src="images/dimension_estampados/paso-2.png">
		</div>
		<div class="prob">
			<p>¿Problemas procesando tu compra?  Contactános y dejanos saber tu inquietud. Tel:(571) 530 97 97 </p>
		</div>
	</div>
	<div class="content-detail">
		<p>Hemos recibido satisfactoriamente tu pedido y un correo de confirmación se te ha enviado a <span class="mail">Ejemplo@correo.com</span> Recuerda que tu pedido se iniciara una vez se reciba la confirmacion de tu pago</p>
		<p> </p>
		<p> </p>
		<p> Ahora, procede a realizar el pago de tu pedido por alguna de las siguientes opciones; </p>
	</div>

	<div id="accordion">
		<h3>Pagos On Line</h3>
		<div>
			<p>
			Haz clicl en el siguiente botón para pagar tu pedido online  puedes pagar con cualquier tarjeta débito o crédito
			</p>
			<p>
			<a href="#"><img src="images/dimension_estampados/logo-pago-1.png"> </a>
			</p>	
		</div>
		<h3>Transferencia Bancolombia</h3>
		<div>
			<p>
			Si tienes cuenta Bancolombia realiza tu transferencia a la cuenta corriente No. 04800641401 a nombre de T-Shirt labs S.A.S
			no olvides enviar una copia de el comprobante de pago al correo info@t-shirtlab.com
			</p>

			<p> <img src="images/dimension_estampados/logo-pago-2.png"> </p>
		</div>
		<h3>Consignación en efectivo</h3>
		<div>
			<p>
			Puedes consignar en efectivo en cualquier punto VIA_BALOTO a la cuenta corriente Citybank No. 1000185104 a nombre de T-Shirt labs S.A.S
			no olvides enviar una copia de el comprobante de pago al correo info@t-shirtlab.com
			</p>
			<p> <img src="images/dimension_estampados/logo-pago-3.png"> 
			<img src="images/dimension_estampados/logo-pago-4.png"> 
			<img src="images/dimension_estampados/logo-pago-5.png"> </p>

		</div>
	</div>

</div>	