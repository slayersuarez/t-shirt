 	<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_anuncios
 * @copyright	SiantetIngenieria . All rights reserved.
 */

defined('_JEXEC') or die;
?>


<div id="carrito">

	<div class="head_detail">
		<div class="product_name">
			<h1> Nombre de el producto </h1>
			<div class="ruta">
				<span class="ruta_page">Inicio/camisetas y otras prendas/camisetas/Camisetas de hombre</span>	
			</div>
		</div>
		<div class="pasos">
			<img src="images/dimension_estampados/paso-2.png">
		</div>
		<div class="prob">
			<p>¿Problemas procesando tu compra?  Contactános y dejanos saber tu inquietud. Tel:(571) 530 97 97 </p>
		</div>

	</div>

	<form id="carrito_detail" action="<?php echo JRoute::_(''); ?>" method="post">
		<table border="0" cellspacing="1" cellpadding="1" class="productos" width="100%">
		<tbody>
			<tr>
				<td class="titles_products"><span>Imagen</span></td>
				<td class="titles_products"><span>Descripción de el Producto </span></td>
				<td class="titles_products"><span>Unidad </span></td>
				<td class="titles_products"><span>Total </span></td>
			</tr>
			<tr>
				<td class="all product_images"><img src="images/dimension_estampados/bolsillo.png"></td>
				<td class="all product_description"> 

					<span class="titulo"> Camiseta WOW negro cuello redondo </span>
					<span class="referencia"> Referencia: CGHRMNE </span>
					<span class="descripcion"> Estampado bolsillo doblecarta </span>
					<a class="delete" href="#"> Eliminar </a>

				 </td>
				<td class="all product_price"> <span class="price"> $34.000</span> </td>
				<td class="all price_total"> <span class="all_price"> $340.000 </span></td>
			</tr>
		</tbody>
		</table>

		<table border="0" class="totales">
			<tbody>
			<tr>
				<td>I.V.A 16%:</td>
				<td> <span class="iva">$56.000</span> </td>
			</tr>
			<tr>
				<td>Subtotal con I.V.A: </td>
				<td><span class="sub_total_iva">$396.000</span> </td>
			</tr>
			<tr>
				<td>Envió:</td>
				<td><span class="price_envio">$0</span></td>
			</tr>
			<tr>
				<td> TOTAL </td>
				<td><span class="total">$396.000</span> </td>
			</tr>
			</tbody>
		</table>
		<div class="opciones">
			<a  href="#" id="continuar">Continuar</a>

			<a  href="index.php" id="seguir">Seguir Comprando</a>
			<span>Haz Click para ingresar tu dirección de envio</span>
		</div>
	</form>	
</div>	