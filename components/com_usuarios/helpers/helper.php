<?php
/**
 * Helper class for anuncios component
 *
 * 
 */
class UsuariosHelper {

    public static function getDepartamentos()  {

		$db = JFactory::getDbo();

    	$query = $db->getQuery(true);
    	$query->select('id as value, nombre as text');
    	$query->from('#__departamentos');
    	
    	$db->setQuery( $query );

    	return $db->loadObjectList();
    }

    public static function getCiudad( $id )  {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__municipios');
        $query->where( 'id ='. $db->quote( $id ) );

        $db->setQuery( $query );

        return $db->loadObject();
    }

    public static function getDepartamento( $id )  {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__departamentos');
        $query->where( 'id ='. $db->quote( $id ) );

        
        $db->setQuery( $query );

        return $db->loadObject();
    }



    public static function getUser()  {

        $db = JFactory::getDbo();

        $user = JFactory::getUser();


        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__users');
        $query->where( 'id = '. $db->quote( $user->id ) );
        
        $db->setQuery( $query );

        return $db->loadObject();
    }


}
?>