<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<div class="registration">

	<div class="info-top">
		<h2>Registro de Clientes Nuevos</h2>
		<p>
			Al registrarse podrá A) Visualizar los precios de los diferentes productos, B) Acceder a descuentos X volumen, C) Realizar compras en línea y D) Recibir ofertas únicas.
		</p>
	</div>

	<ul class="info-registro">
		<li><span class="title">Datos generales</span></li>
		<li><span class="required">* </span><span class="message-required">Todos los campos son obligatorios</span></li>
	</ul>

	<ul class="cuenta">
		<li><span>Seleccione el tipo de cliente</span></li>
		<li><input type="radio" id="empresa" name="cliente" value="1"/><label>Empresa</label></li>
		<li><input type="radio" id="persona" name="cliente" value="2" checked/><label>Persona</label></li>
	</ul>

	<form id="member-registration-persona">
		<ul class="datos-personales">
			<li><label>Nombre *</label></li>
			<li><label>Apellidos (2) *</label></li>
			<li><input type="text" name="nombre" /></li>
			<li><input type="text" name="apellidos" /></li>
			<li><label>E-mail *</label></li>
			<li><label>Confirmar E-mail *</label></li>
			<li><input type="text" name="email" /></li>
			<li><input type="text" name="confirmar_email" /></li>
			<li><label>Contraseña *</label></li>
			<li><label>Confirmar Contraseña *</label></li>
			<li><input type="password" name="password" /></li>
			<li><input type="password" name="confirmar_password" /></li>
		</ul>

		<span class="title">Dirección y datos de facturación</span>

		<ul class="datos-facturacion-two">
			<li><label>Cédula de ciudadania *</label></li>
			<li><input type="text" name="cedula" /></li>
		</ul>

		<ul class="datos-facturacion-one">
			<li><label>Dirección *</label></li>
			<li><label>Departamento *</label></li>
			<li><label>Ciudad *</label></li>
			<li><label>Tel. Fijo *</label></li>
			<li><label>Celular *</label></li>
			<li><input type="text" name="direccion" /></li>
			<li>
				<select size="0" class="inputbox" id="departamento" name="departamento" style="width: 162px;">
					<option value="">Seleccione Departamento</option>
					<?php 

					$departamentos = UsuariosHelper::getDepartamentos();

					foreach ($departamentos as $key => $departamento) {
					?>
						<option value="<?= $departamento->value ?>"><?= $departamento->text ?></option>
					<?php
					}
					?>
				</select>
			</li>
			<li>
				<select name="ciudad" id="ciudad" disabled="true">
					<option value="">Seleccione la Ciudad</option>
				</select>
			</li>
			<li><input type="text" name="telefono" /></li>
			<li><input type="text" name="celular" /></li>
		</ul>	
		

		<span class="title">Condiciones legales</span>
		<p>Leí y estoy de acuerdo con las condiciones legales de T-Shirt Online</p>

		<ul class="condiciones">
			<li><input type="radio" name="terminos" value="1"/>Estoy de acuerdo</li>
			<li><input type="radio" name="terminos" value="2" checked/>No Estoy de acuerdo</li>
		</ul>

		<input class="enviar-button" type="submit" value="Enviar" />
	</form>
</div>