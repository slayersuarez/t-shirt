<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$user = JFactory::getUser();
$app = JFactory::getApplication();
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root


if (! $user->guest ) {

	$usuario = UsuariosHelper::getUser( $user->id );

	if( $usuario->tipo == '2' ){
		$app->redirect($url.'index.php/cuenta/?layout=persona');
	}else{
		$app->redirect($url.'index.php/cuenta/?layout=empresa');
	}
	
}else{
	$app->redirect($url.'index.php/component/users/?view=login','Por favor inicie sesión', 'error');
}

?>
