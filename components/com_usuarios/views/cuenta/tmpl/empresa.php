<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');


$usuario = UsuariosHelper::getUser( $user->id );

$ciudad = UsuariosHelper::getCiudad( $usuario->ciudad );

$departament = UsuariosHelper::getDepartamento( $ciudad->id_departamento );


?>
<div class="registration">

	<div class="info-top" style="width: 100%;">
		<h2>Edición de cuenta</h2>
		
	</div>

	<ul class="info-registro">
		<li><span class="title">Datos generales</span></li>
		<li><span class="required">* </span><span class="message-required">Todos los campos son obligatorios</span></li>
	</ul>

	<form id="member-edit-empresa">
		<ul class="datos-personales">
			<li><label>Empresa *</label></li>
			<li><label>Contacto (Nombre & Apellido) *</label></li>
			<li><input type="text" name="empresa" value="<?= $usuario->empresa ?>"/></li>
			<li><input type="text" name="contacto" value="<?= $usuario->contacto ?>"/></li>
		</ul>
		<ul class="datos-personales">
			<li style="width:100%;"><label>E-mail *</label></li>
			<li><input type="text" name="email" value="<?= $usuario->email ?>" readonly="readonly"/></li>
		</ul>
		<ul class="datos-personales">
			<li><label>Contraseña *</label></li>
			<li><label>Confirmar Contraseña *</label></li>
			<li><input type="password" name="password"/></li>
			<li><input type="password" name="confirmar_password" /></li>
		</ul>

		<span class="title">Dirección y datos de facturación</span>

		<ul class="datos-facturacion-two">
			<li><label>NIT *</label></li>
			<li><input type="text" name="nit" value="<?= $usuario->identificacion ?>"/></li>
		</ul>

		<ul class="datos-facturacion-one">
			<li><label>Dirección *</label></li>
			<li><label>Departamento *</label></li>
			<li><label>Ciudad *</label></li>
			<li><label>Tel. Fijo *</label></li>
			<li><label>Celular *</label></li>
			<li><input type="text" name="direccion" value="<?= $usuario->direccion ?>" /></li>
			<li>
				<select size="0" class="inputbox" id="departamento" name="departamento" style="width: 162px;">
					<option value="">Seleccione Departamento</option>
					<?php 

					$departamentos = UsuariosHelper::getDepartamentos();

					foreach ($departamentos as $key => $departamento) {

						$selected = '';

						if ( $departament->id == $departamento->value ) {
							$selected = 'selected';
						}
					?>
						<option <?= $selected ?> value="<?= $departamento->value ?>"><?= $departamento->text ?></option>
					<?php
					}
					?>
				</select>
			</li>
			<li>
				<select name="ciudad" id="ciudad" disabled="true">
					<option value="">Seleccione la Ciudad</option>
				</select>
			</li>
			<li><input type="text" name="telefono" value="<?= $usuario->telefono ?>"/></li>
			<li><input type="text" name="celular" value="<?= $usuario->celular ?>"/></li>
		</ul>

		<input type="hidden" value="<?= $usuario->ciudad ?>" id="city"/>
		<input type="hidden" name="id" value="<?= $usuario->id ?>"/>

		<input class="enviar-button" type="submit" value="Enviar" />
	</form>
</div>