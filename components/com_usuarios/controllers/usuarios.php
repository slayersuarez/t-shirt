<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class UsuariosControllerUsuarios extends JController{

	public function getCiudades(){

		$id = JRequest::getVar('id');
		$response = new stdClass();

		$userModel = $this->getModel('usuarios');

		$municipios = $userModel->getCiudades( $id );

		$response->municipios = $municipios;
		echo json_encode($response);
		die;

	}

	public function savePersona(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    $data = $userModel->getUser( $usuario['email'] );

	    if ( !empty ($data) ) {
	    	$response->status = 300;
	    	$response->message = 'El usuario '.$usuario['email'].' ya existe.';
	    	echo json_encode($response);
			die;
	    }

		//generate password
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < 32; $i++) {
	        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
	    }

	    // generates joomla password
	    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'name' => $usuario['nombre']
	    	,	'username' => $usuario['email']
	    	,	'email' => $usuario['email']
	    	,	'password' => $usuario['password']
	    	,	'block' => '0'
	    	,	'registerDate' => $date
	    	,	'identificacion' => $usuario['cedula']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'apellidos' => $usuario['apellidos']
	    	,	'tipo' =>  '2'

	    );

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $id = $userModel->insertId;

	    // merge group registered with user
	    $groupArgs = array('user_id' => $id , 'group_id' => '2' );
	    $groupModel = $this->getModel('groups');
	    $groupModel->instance( $groupArgs );
	    $groupModel->saveGroup();


	    $response->status = 200;
	    $response->message = 'Registro correcto.';
		echo json_encode($response);
		die;

	}

	public function editPersona(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'id' => $usuario['id']
	    	,	'name' => $usuario['nombre']
	    	,	'identificacion' => $usuario['cedula']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'apellidos' => $usuario['apellidos']

	    );

	    if( ! empty( $usuario[ 'password' ] ) ) {
			
			//generate password
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < 32; $i++) {
		        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
		    }

		    // generates joomla password
		    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

		    $pass = array('password' => $usuario[ 'password' ] );

		    $args = array_merge( $array, $pass );
	    }

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

		$response->status = 200;
	    $response->message = 'Usuario editado correctamente.';
		echo json_encode($response);
		die;
	}

	public function saveEmpresa(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    $data = $userModel->getUser( $usuario['email'] );

	    if ( !empty ($data) ) {
	    	$response->status = 300;
	    	$response->message = 'El usuario '.$usuario['email'].' ya existe.';
	    	echo json_encode($response);
			die;
	    }

		//generate password
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < 32; $i++) {
	        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
	    }

	    // generates joomla password
	    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'name' => $usuario['empresa']
	    	,	'username' => $usuario['email']
	    	,	'email' => $usuario['email']
	    	,	'password' => $usuario['password']
	    	,	'block' => '0'
	    	,	'registerDate' => $date
	    	,	'identificacion' => $usuario['nit']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'empresa' =>  $usuario['empresa']
	    	,	'contacto' =>  $usuario['contacto']
	    	,	'tipo' =>  '2'

	    );

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $id = $userModel->insertId;

	    // merge group registered with user
	    $groupArgs = array('user_id' => $id , 'group_id' => '2' );
	    $groupModel = $this->getModel('groups');
	    $groupModel->instance( $groupArgs );
	    $groupModel->saveGroup();


	    $response->status = 200;
	    $response->message = 'Registro correcto.';
		echo json_encode($response);
		die;

	}

	public function editEmpresa(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'id' => $usuario['id']
	    	,	'name' => $usuario['empresa']
	    	,	'identificacion' => $usuario['nit']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'apellidos' => $usuario['apellidos']
	    	,	'empresa' =>  $usuario['empresa']
	    	,	'contacto' =>  $usuario['contacto']
	    );

	    if( ! empty( $usuario[ 'password' ] ) ) {
			
			//generate password
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < 32; $i++) {
		        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
		    }

		    // generates joomla password
		    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

		    $pass = array('password' => $usuario[ 'password' ] );

		    $args = array_merge( $args, $pass );
	    }



	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

		$response->status = 200;
	    $response->message = 'Usuario editado correctamente.';
		echo json_encode($response);
		die;
	}
	
}
?>