<?php

defined('_JEXEC') or die;

JHtml::_('behavior.framework', true);

$app = JFactory::getApplication();

$doc = JFactory::getDocument();

?>

<!DOCTYPE HTML>

<html>

	<head>
		<jdoc:include type="head" />
		<meta charset="utf-8"/>

		<!-- Estos son los estilos -->
		<link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">

		<!--Estas son las fuentes-->
		<link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Allan' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

		<!--SCRIPTS-->
	</head>

	<body>
		<header>
			<div class="contenedor-header">
				<div class="header-arriba">
					<div class="logo">
						<img src="images/logo.png" alt="Logo"/>
					</div>
					<jdoc:include type="modules" name="login" style="xhtml"/>
					<jdoc:include type="modules" name="carrito" style="xhtml"/>
				</div>
				<nav>
					<div class="menu1">
						<jdoc:include type="modules" name="menu1" style="xhtml"/>
					</div>
				</nav>
				<div class="banner">
					<jdoc:include type="modules" name="banner" style="xhtml"/>
				</div>
			</div>
		</header>
		<main>
			<jdoc:include type="modules" name="imagen-fondo" style="xhtml"/>
			<div class="content-all">
				<div class="imagenes-main">
					<jdoc:include type="modules" name="img-main" style="xhtml"/>
				</div>
				<div class="registro">
					<jdoc:include type="modules" name="registro" style="xhtml"/>
				</div>
			</div>
		</main>	
		<footer>
			<div class="contenedor-footer">
				<div class="info">
					<jdoc:include type="modules" name="info" style="xhtml"/>
				</div>
				<div class="copy">
					<span class="sainet">
						<a target="_blank" href="http://www.creandopaginasweb.com">
							Página web desarrollada por <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logoverde.png">
						</a>
					</span>
				</div>
			</div>
		</footer>
	</body>
</html>