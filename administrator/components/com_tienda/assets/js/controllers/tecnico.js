/**
*
* Controller for { Tecnico }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var TecnicoController = function( a ){
		
		// atributes, selector or global vars
		this.excel = '';
		this.pdf = '';
		this.excels = [];
		
	};
	
	TecnicoController.prototype ={
			

			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
			parseExcel: function( data ){

				view.onBeforeParse( this.excels );

				var count = this.excels.length
				,	_this = this
				,	_data = data;

				// For each file in excels array, make a request to save the excel data
				for (var i = 0; i < this.excels.length ; i++ ) {
					
					data.excel = this.excels[i];

					var success = function( data ){

						view.onCompleteParse( data );

						count--;

						_this.deleteTemp( count, _data );
					};

					var error = function ( XMLHttpRequest, textStatus, errorThrown ) {
                        return view.onError( XMLHttpRequest, textStatus, errorThrown );   
                    };

					model.parseExcel( success, error,  data );
				}
				
			}

			/**
			* Deletes the uploaded file
			*
			*/
		,	deleteTemp: function( count, data ){

				// At the end delete the excels file
				if( count <= 0 ){
					
					view.onBeforeDeleteTemp();

					var completeDeleteTemp = function( data ){

						view.onCompleteDeleteTemp( data );
					};

					return model.deleteTemp( completeDeleteTemp, data  );
				}
			}

		,	truncate: function( _data ){

				view.onBeforeTruncate();

				var _this = this;

				var success = function( data ){

					view.onCompleteTruncate( data );

					if( data.status == 200 )
						_this.parseExcel( _data );
				};

				return model.truncate( success, _data );
			}


			/**
			* Verifies the pdf file with some data
			*
			*/
		,	verifyMatricula: function( fileName ){

				var success = function( data ){

					view.renderPDFLog( data );
				};

				return model.verifyMatricula( success, fileName );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.TecnicoController = new TecnicoController();
	
})( jQuery, this, this.document, this.TecnicoModel, this.TecnicoView, this.Misc, undefined );