<?php
/**
 * Export userinfo
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: default_userinfo.php 2438 2013-05-27 20:14:42Z Roland $
 */

defined( '_JEXEC' ) or die;
?>
<fieldset>
	<legend><?php echo JText::_('COM_CSVI_OPTIONS'); ?></legend>
	<ul>
		<li><div class="option_label"><?php echo $this->form->getLabel('userinfo_address', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('userinfo_address', 'userinfo'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('vendors', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('vendors', 'userinfo'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('permissions', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('permissions', 'userinfo'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('userinfomdatestart', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('userinfomdatestart', 'userinfo'); ?> <?php echo $this->form->getInput('userinfomdateend', 'userinfo'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('blocked', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('blocked', 'userinfo'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('activated', 'userinfo'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('activated', 'userinfo'); ?></div></li>
	</ul>
</fieldset>
<div class="clr"></div>