<?php
/**
 * Export K2 Content
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: default_product.php 2052 2012-08-02 05:44:47Z RolandD $
 */

defined('_JEXEC') or die;
?>
<fieldset>
	<legend><?php echo JText::_('COM_CSVI_OPTIONS'); ?></legend>
	<ul>
		<li><div class="option_label"><?php echo $this->form->getLabel('item_language', 'general'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('item_language', 'general'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('exportsef', 'product'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('exportsef', 'product'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('category_separator', 'general'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('category_separator', 'general'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('item_categories', 'item'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('item_categories', 'item'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('image_url', 'item'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('image_url', 'item'); ?></div></li>
		<li><div class="option_label"><?php echo $this->form->getLabel('image_url_thumb', 'item'); ?></div>
			<div class="option_value"><?php echo $this->form->getInput('image_url_thumb', 'item'); ?></div></li>
	</ul>
</fieldset>
<div class="clr"></div>