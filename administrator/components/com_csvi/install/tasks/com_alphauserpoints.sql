DELETE FROM `#__csvi_template_tables` WHERE `component` = 'com_alphauserpoints';
INSERT IGNORE INTO `#__csvi_template_tables` (`template_type_name`, `template_table`, `component`) VALUES
('userpointexport', 'userpointeexport', 'com_alphauserpoints'),
('userpointexport', 'alpha_userpoints', 'com_alphauserpoints'),
('userpointexport', 'users', 'com_alphauserpoints'),
('userpointimport', 'userpointeexport', 'com_alphauserpoints'),
('userpointimport', 'alpha_userpoints', 'com_alphauserpoints'),
('userpointimport', 'users', 'com_alphauserpoints');

DELETE FROM `#__csvi_template_types` WHERE `component` = 'com_alphauserpoints';
INSERT IGNORE INTO `#__csvi_template_types` (`template_type_name`, `template_type`, `component`, `url`, `options`) VALUES
('userpointexport', 'export', 'com_alphauserpoints', 'index.php?option=com_alphauserpoints&task=statistics', 'file,fields,layout,email,limit'),
('userpointimport', 'import', 'com_alphauserpoints', 'index.php?option=com_akeebasubs&task=statistics', 'file,fields,limit');