/* K2 item export */
INSERT IGNORE INTO `#__csvi_available_fields` (`csvi_name`, `component_name`, `component_table`, `component`) VALUES
('custom', 'custom', 'itemexport', 'com_k2'),
('category_path', 'category_path', 'itemexport', 'com_k2'),
('image', 'image', 'itemexport', 'com_k2'),
('item_url', 'item_url', 'itemexport', 'com_k2'),
('image_url', 'image_url', 'itemexport', 'com_k2'),
('image_url_thumb', 'image_url_thumb', 'itemexport', 'com_k2'),
('metadata_robot', 'metadata_robot', 'itemexport', 'com_k2'),
('metadata_author', 'metadata_author', 'itemexport', 'com_k2'),

/* K2 item import */
('skip', 'skip', 'itemimport', 'com_k2'),
('combine', 'combine', 'itemimport', 'com_k2'),
('image', 'image', 'itemimport', 'com_k2'),
('category_path', 'category_path', 'itemimport', 'com_k2'),
('metadata_robot', 'metadata_robot', 'itemimport', 'com_k2'),
('metadata_author', 'metadata_author', 'itemimport', 'com_k2'),

/* K2 category export */
('custom', 'custom', 'categoryexport', 'com_k2'),
('category_path', 'category_path', 'categoryexport', 'com_k2'),
('parent_category_path', 'parent_category_path', 'categoryexport', 'com_k2'),
('inheritFrom', 'inheritFrom', 'categoryexport', 'com_k2'),

/* K2 category import */
('skip', 'skip', 'categoryimport', 'com_k2'),
('combine', 'combine', 'categoryimport', 'com_k2'),
('category_path', 'category_path', 'categoryimport', 'com_k2'),
('category_delete', 'category_delete', 'categoryimport', 'com_k2'),
('inheritFrom', 'inheritFrom', 'categoryimport', 'com_k2'),

/* K2 extra fields export */
('custom', 'custom', 'extrafieldexport', 'com_k2'),
('option_name', 'option_name', 'extrafieldexport', 'com_k2'),
('option_value', 'option_value', 'extrafieldexport', 'com_k2'),
('option_target', 'option_target', 'extrafieldexport', 'com_k2'),
('option_editor', 'option_editor', 'extrafieldexport', 'com_k2'),
('option_rows', 'option_rows', 'extrafieldexport', 'com_k2'),
('option_cols', 'option_cols', 'extrafieldexport', 'com_k2'),
('alias', 'alias', 'extrafieldexport', 'com_k2'),
('required', 'required', 'extrafieldexport', 'com_k2'),
('showNull', 'showNull', 'extrafieldexport', 'com_k2'),
('displayInFrontEnd', 'displayInFrontEnd', 'extrafieldexport', 'com_k2'),
('group_name', 'group_name', 'extrafieldexport', 'com_k2'),

/* K2 extra fields import */
('skip', 'skip', 'extrafieldimport', 'com_k2'),
('combine', 'combine', 'extrafieldimport', 'com_k2'),
('option_name', 'option_name', 'extrafieldimport', 'com_k2'),
('option_value', 'option_value', 'extrafieldimport', 'com_k2'),
('option_target', 'option_target', 'extrafieldimport', 'com_k2'),
('option_editor', 'option_editor', 'extrafieldimport', 'com_k2'),
('option_rows', 'option_rows', 'extrafieldimport', 'com_k2'),
('option_cols', 'option_cols', 'extrafieldimport', 'com_k2'),
('alias', 'alias', 'extrafieldimport', 'com_k2'),
('required', 'required', 'extrafieldimport', 'com_k2'),
('showNull', 'showNull', 'extrafieldimport', 'com_k2'),
('displayInFrontEnd', 'displayInFrontEnd', 'extrafieldimport', 'com_k2'),
('group_name', 'group_name', 'extrafieldimport', 'com_k2');

INSERT IGNORE INTO `#__csvi_available_fields` (csvi_name, component_name, component_table, component)
	(SELECT TRIM(name), TRIM(name), 'itemimport', 'com_k2'
		FROM `#__k2_extra_fields`
	);

INSERT IGNORE INTO `#__csvi_available_fields` (csvi_name, component_name, component_table, component)
	(SELECT TRIM(name), TRIM(name), 'itemexport', 'com_k2'
		FROM `#__k2_extra_fields`
	);