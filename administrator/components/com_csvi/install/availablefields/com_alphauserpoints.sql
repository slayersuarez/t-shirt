/* Alpha user points export */
INSERT IGNORE INTO `#__csvi_available_fields` (`csvi_name`, `component_name`, `component_table`, `component`) VALUES
('custom', 'custom', 'userpointexport', 'com_alphauserpoints'),
('name', 'name', 'userpointexport', 'com_alphauserpoints'),
('username', 'username', 'userpointexport', 'com_alphauserpoints'),
('email', 'email', 'userpointexport', 'com_alphauserpoints'),
('password', 'password', 'userpointexport', 'com_alphauserpoints'),

/* Alpha user points import */
('skip', 'skip', 'userpointimport', 'com_alphauserpoints'),
('name', 'name', 'userpointimport', 'com_alphauserpoints'),
('username', 'username', 'userpointimport', 'com_alphauserpoints'),
('email', 'email', 'userpointimport', 'com_alphauserpoints'),
('password', 'password', 'userpointimport', 'com_alphauserpoints');