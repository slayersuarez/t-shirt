<?php
/**
 * Alpha user points table
 *
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: coupons.php 2273 2013-01-03 16:33:30Z RolandD $
 */

// No direct access
defined('_JEXEC') or die;

/**
 * @package CSVI
 */
class TableUserpoints extends JTable {

	/**
	 * Table constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		4.0
	 */
	public function __construct($db) {
		parent::__construct('#__alpha_userpoints', 'id', $db );
	}

	/**
	 * Check if a user already exists
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function check() {
		if (isset($this->id)) return true;
		else {
			$jinput = JFactory::getApplication()->input;
			$csvilog = $jinput->get('csvilog', null, null);
			$db = JFactory::getDbo();
			$query = $db->getQuery(true)
				->select($db->qn($this->_tbl_key))
				->from($db->qn($this->_tbl))
				->where($db->qn('userid').' = '.$db->q($this->userid));
			$db->setQuery($query);
			$csvilog->addDebug(JText::_('COM_CSVI_CHECK_USERPOINT_EXISTS'), true);
			$db->query();
			if ($db->getAffectedRows() > 0) {
				$this->id = $db->loadResult();
				return true;
			}
			else return false;
		}
	}

	/**
	 * Reset the table fields, need to do it ourselves as the fields default is not NULL
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		4.0
	 */
	public function reset() {
		// Get the default values for the class from the table.
		foreach ($this->getFields() as $k => $v) {
			// If the property is not private, reset it.
			if (strpos($k, '_') !== 0) {
				$this->$k = NULL;
			}
		}
	}
}