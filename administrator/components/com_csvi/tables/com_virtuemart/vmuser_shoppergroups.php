<?php
/**
 * VirtueMart user shoppergroups table
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: vmuser_shoppergroups.php 2438 2013-05-27 20:14:42Z Roland $
 */

// No direct access
defined('_JEXEC') or die;

/**
* @package CSVI
 * @subpackage Tables
 */
class TableVmuser_shoppergroups extends JTable {

	/**
	 * Table constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		4.0
	 */
	public function __construct($db) {
		parent::__construct('#__virtuemart_vmuser_shoppergroups', 'id', $db );
	}

	/**
	 * Check if a relation already exists
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		4.0
	 */
	public function check() {
		if (!empty($this->virtuemart_user_id) && !empty($this->virtuemart_shoppergroup_id)) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->qn($this->_tbl_key));
			$query->from($db->qn($this->_tbl));
			$query->where($db->qn('virtuemart_user_id').' = '.$db->q($this->virtuemart_user_id));
			$query->where($db->qn('virtuemart_shoppergroup_id').' = '.$db->q($this->virtuemart_shoppergroup_id));
			$db->setQuery($query);
			$this->id = $db->loadResult();
		}
	}

	/**
	 * Reset the keys including primary key
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		4.0
	 */
	public function reset() {
		// Get the default values for the class from the table.
		foreach ($this->getFields() as $k => $v) {
			// If the property is not private, reset it.
			if (strpos($k, '_') !== 0) {
				$this->$k = NULL;
			}
		}
	}
}