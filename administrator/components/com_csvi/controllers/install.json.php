<?php
/**
 * Install controller
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: install.json.php 2504 2013-10-01 13:58:13Z Roland $
 */

defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controller');

/**
 * Install Controller
 *
 * @package    CSVIVirtueMart
 */
class CsviControllerInstall extends JControllerLegacy {

	/**
	 * Upgrade CSVI VirtueMart
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function upgrade() {
		// Create the view object
		$view = $this->getView('install', 'json');

		// Standard model
		$install = $this->getModel( 'install', 'CsviModel' );
		$avfields = $this->getModel( 'availablefields', 'CsviModel' );
		$maint = $this->getModel( 'maintenance', 'CsviModel' );
		$view->setModel($install, true );
		$view->setModel($avfields);
		$view->setModel($maint);

		$view->display();
	}
}