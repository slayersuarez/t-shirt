<?php
/**
 * Categories helper file
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: com_virtuemart.php 2052 2012-08-02 05:44:47Z RolandD $
 */

defined('_JEXEC') or die;

class Com_Categories {

	private $_csvidata = null;

	/**
	 * Constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		5.3
	 */
	public function __construct() {
		$jinput = JFactory::getApplication()->input;
		$this->_csvidata = $jinput->get('csvifields', null, null);
	}

	/**
	 * Get the category ID based on it's path
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param		string	$category_path	The path of the category
	 * @param		string	$extension		The extension the category belongs to
	 * @return 		int	the ID of the category
	 * @since 		5.3
	 */
	public function getCategoryId($category_path, $extension) {
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->from($db->qn('#__categories'))->where($db->qn('extension').' = '.$db->q($extension))->where($db->qn('path').' = '.$db->q($category_path));
		$db->setQuery($query);
		$catid = $db->loadResult();
		$csvilog->addDebug('Find category ID', true);
		$this->_csvidata->set('id', $catid);
		return $catid;
	}
	
	public function transliterate($category_path) {
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$template = $jinput->get('template', null, null);
		
		// Transliterate
		$lang = new JLanguage($template->get('language', 'general', '', null, 0, false));
		$str = $lang->transliterate($category_path);
		
		// Trim white spaces at beginning and end of alias and make lowercase
		$str = trim(JString::strtolower($str));
		
		// Remove any duplicate whitespace, and ensure all characters are alphanumeric
		$str = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', $str);
		
		// Trim dashes at beginning and end of alias
		$path = trim($str, '-');
		
		// If we are left with an empty string, make a date with random number
		if (trim(str_replace('-', '', $str)) == '') {
			$jdate = JFactory::getDate();
			$str = $jdate->format("Y-m-d-h-i-s").mt_rand();
		}
		
		return $str;
	}
}