<?php
/**
 * Content import
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: couponimport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

class CsviModelContentimport extends CsviModelImportfile {

	// Private tables
	/** @var object contains the properties table */
	private $_content = null;
	private $_categorymodel = null;


	// Public variables
	public $helper = null;
	public $id = null;
	public $alias = null;
	public $catid = null;
	public $category_path = null;
	public $show_title = null;
	public $link_titles = null;
	public $show_intro = null;
	public $show_category = null;
	public $link_category = null;
	public $show_parent_category = null;
	public $link_parent_category = null;
	public $show_author = null;
	public $link_author = null;
	public $show_create_date = null;
	public $show_modify_date = null;
	public $show_publish_date = null;
	public $show_item_navigation = null;
	public $show_icons = null;
	public $show_print_icon = null;
	public $show_email_icon = null;
	public $show_vote = null;
	public $show_hits = null;
	public $show_noauth = null;
	public $urls_position = null;
	public $alternative_readmore = null;
	public $article_layout = null;
	public $show_publishing_options = null;
	public $show_article_options = null;
	public $show_urls_images_backend = null;
	public $show_urls_images_frontend = null;
	public $meta_robots = null;
	public $meta_author = null;
	public $meta_rights = null;
	public $meta_xreference = null;

	/**
	 * Constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.4
	 */
	public function __construct() {
		parent::__construct();
		// Load the tables that will contain the data
		$this->_loadTables();
		$this->loadSettings();
    }

	/**
	 * Here starts the processing
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getStart() {
		// Load the data
		$this->loadData();

		// Get the logger
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);

		// Load the helper
		$this->helper = new Com_Content();

		// Find the content id
		$this->id = $this->helper->getContentId();

		// Load the current content data
		$this->_content->load($this->id);

		// Process data
		foreach ($this->csvi_data as $name => $fields) {
			foreach ($fields as $details) {
				$value = $details['value'];
				// Check if the field needs extra treatment
				switch ($name)
				{
					default:
						$this->$name = $value;
						break;
				}
			}
		}

		// There must be an alias and catid or category_path
		if (empty($this->alias) && (empty($this->catid) && empty($this->category_path))) return false;

		// All good
		return true;
	}

	/**
	 * Process each record and store it in the database
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getProcessRecord() {
		$jinput = JFactory::getApplication()->input;
		$template = $jinput->get('template', null, null);
		$csvilog = $jinput->get('csvilog', null, null);

		if ($this->id && !$template->get('overwrite_existing_data', 'general')) {
			$csvilog->addDebug(JText::sprintf('COM_CSVI_DATA_EXISTS_CONTENT', $this->alias));
			$csvilog->AddStats('skipped', JText::sprintf('COM_CSVI_DATA_EXISTS_CONTENT', $this->alias));
		}
		else {
			// Check if there is a category ID
			if (empty($this->catid) && $this->category_path) {
				// Find the category ID
				$this->catid = $this->helper->getCategoryId($this->category_path);
			}

			// Check if we have a title
			if (empty($this->_content->title)) $this->_content->title = $this->alias;

			// Check for attributes
			if (!isset($this->attribs))
			{
				$attributeFields =
					array(
						'show_title',
						'link_titles',
						'show_intro',
						'show_category',
						'link_category',
						'show_parent_category',
						'link_parent_category',
						'show_author',
						'link_author',
						'show_create_date',
						'show_modify_date',
						'show_publish_date',
						'show_item_navigation',
						'show_icons',
						'show_print_icon',
						'show_email_icon',
						'show_vote',
						'show_hits',
						'show_noauth',
						'urls_position',
						'alternative_readmore',
						'article_layout',
						'show_publishing_options',
						'show_article_options',
						'show_urls_images_backend',
						'show_urls_images_frontend'

					);

				// Load the current attributes
				$attributes = json_decode($this->_content->attribs);

				foreach ($attributeFields as $field)
				{
					if (!is_null($this->$field))
					{
						if ($this->$field == '*')
						{
							$attributes->$field = '';
						}
						else
						{
							$attributes->$field = $this->$field;
						}
					}
				}

				// Store the new attributes
				$this->_content->attribs = json_encode($attributes);
			}

			// Check for meta data
			if (!isset($this->metadata))
			{
				$metadataFields =
					array(
						'meta_robots',
						'meta_author',
						'meta_rights',
						'meta_xreference'
					);

				// Load the current attributes
				$metadata = json_decode($this->_content->metadata);

				foreach ($metadataFields as $field)
				{
					if (!is_null($this->$field))
					{
						if ($this->$field == '*')
						{
							$metadata->$field = '';
						}
						else
						{
							$newField = str_ireplace('meta_', '', $field);
							$metadata->$newField = $this->$field;
						}
					}
				}

				// Store the new attributes
				$this->_content->metadata = json_encode($metadata);
			}

			// Bind the data
			$this->_content->save($this);
		}

		// Clean the tables
		$this->cleanTables();
	}

	/**
	 * Load the coupon related tables
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		3.0
	 */
	private function _loadTables() {
		$this->_content = $this->getTable('content');
	}

	/**
	 * Cleaning the coupon related tables
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return
	 * @since 		3.0
	 */
	protected function cleanTables() {
		$this->_content->reset();

		// Clean local variables
		$class_vars = get_class_vars(get_class($this));
		foreach ($class_vars as $name => $value) {
			if (substr($name, 0, 1) != '_') {
				$this->$name = $value;
			}
		}
	}
}