<?php
/**
 * Joomla Content export class
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: customfieldsexport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

/**
 * Processor for Joomla Content exports
 */
class CsviModelContentExport extends CsviModelExportfile {

	/**
	 * Content export
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return 		void
	 * @since 		3.4
	 */
	public function getStart() {
		// Get some basic data
		$db = JFactory::getDbo();
		$csvidb = new CsviDb();
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$template = $jinput->get('template', null, null);
		$exportclass =  $jinput->get('export.class', null, null);
		$export_fields = $jinput->get('export.fields', array(), 'array');
		$sef = new CsviSef();
		require_once JPATH_SITE . '/components/com_content/helpers/route.php';

		// Build something fancy to only get the fieldnames the user wants
		$userfields = array();
		foreach ($export_fields as $column_id => $field) {
			switch ($field->field_name) {
				case 'category_path':
					$userfields[] = $db->qn('c.catid');
					break;
				case 'article_url':
					$userfields[] = $db->qn('c.id');
					$userfields[] = $db->qn('cat.id', 'catid');
					break;
				case 'show_title':
				case 'link_titles':
				case 'show_intro':
				case 'show_category':
				case 'link_category':
				case 'show_parent_category':
				case 'link_parent_category':
				case 'show_author':
				case 'link_author':
				case 'show_create_date':
				case 'show_modify_date':
				case 'show_publish_date':
				case 'show_item_navigation':
				case 'show_icons':
				case 'show_print_icon':
				case 'show_email_icon':
				case 'show_vote':
				case 'show_hits':
				case 'show_noauth':
				case 'urls_position':
				case 'alternative_readmore':
				case 'article_layout':
				case 'show_publishing_options':
				case 'show_article_options':
				case 'show_urls_images_backend':
				case 'show_urls_images_frontend':
					$userfields[] = $db->qn('c.attribs');
					break;
				case 'meta_robots':
				case 'meta_author':
				case 'meta_rights':
				case 'meta_xreference':
					$userfields[] = $db->qn('c.metadata');
					break;
				case 'custom':
					break;
				default:
					$userfields[] = $db->qn('c.' . $field->field_name);
					break;
			}
		}

		// Build the query
		$userfields = array_unique($userfields);
		$query = $db->getQuery(true);
		$query->select(implode(",\n", $userfields));
		$query->from($db->qn("#__content", "c"));
		$query->leftJoin($db->qn('#__categories', 'cat') . ' ON ' . $db->qn('cat.id') . ' = ' . $db->qn('c.catid'));

		$selectors = array();

		// Filter by published state
		$publish_state = $template->get('publish_state', 'general');
		if ($publish_state != '' && ($publish_state == 1 || $publish_state == 0)) {
			$selectors[] = $db->qn('c.published').' = '.$publish_state;
		}

		// Filter by language
		$language = $template->get('content_language', 'general');
		if ($language != '*') {
			$selectors[] = $db->qn('c.language').' = '.$db->q($language);
		}

		// Filter by category
		$categories = $template->get('content_categories', 'content');
		if ($categories && $categories[0] != '*') {
			$selectors[] = $db->qn('catid')." IN ('".implode("','", $categories)."')";
		}

		// Check if we need to attach any selectors to the query
		if (count($selectors) > 0 ) $query->where(implode("\n AND ", $selectors));

		// Ingore fields
		$ignore = array('custom', 'category_path', 'article_url', 'meta_robots',
			'meta_author',
			'meta_rights',
			'meta_xreference',
			'show_title',
			'link_titles',
			'show_intro',
			'show_category',
			'link_category',
			'show_parent_category',
			'link_parent_category',
			'show_author',
			'link_author',
			'show_create_date',
			'show_modify_date',
			'show_publish_date',
			'show_item_navigation',
			'show_icons',
			'show_print_icon',
			'show_email_icon',
			'show_vote',
			'show_hits',
			'show_noauth',
			'urls_position',
			'alternative_readmore',
			'article_layout',
			'show_publishing_options',
			'show_article_options',
			'show_urls_images_backend',
			'show_urls_images_frontend'
		);

		// Check if we need to group the orders together
		$groupby = $template->get('groupby', 'general', false, 'bool');
		if ($groupby) {
			$filter = $this->getFilterBy('groupby', $ignore);
			if (!empty($filter)) $query->group($filter);
		}

		// Order by set field
		$orderby = $this->getFilterBy('sort', $ignore);
		if (!empty($orderby)) $query->order($orderby);

		// Add a limit if user wants us to
		$limits = $this->getExportLimit();

		// Execute the query
		$csvidb->setQuery($query, $limits['offset'], $limits['limit']);
		$csvilog->addDebug(JText::_('COM_CSVI_EXPORT_QUERY'), true);

		// There are no records, write SQL query to log
		if (!is_null($csvidb->getErrorMsg())) {
			$this->addExportContent(JText::sprintf('COM_CSVI_ERROR_RETRIEVING_DATA', $csvidb->getErrorMsg()));
			$this->writeOutput();
			$csvilog->AddStats('incorrect', $csvidb->getErrorMsg());
		}
		else {
			$logcount = $csvidb->getNumRows();
			$jinput->set('logcount', $logcount);
			if ($logcount > 0) {
				while ($record = $csvidb->getRow()) {
					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') $this->addExportContent($exportclass->NodeStart());
					foreach ($export_fields as $column_id => $field) {
						$fieldname = $field->field_name;
						// Add the replacement
						if (isset($record->$fieldname)) $fieldvalue = CsviHelper::replaceValue($field->replace, $record->$fieldname);
						else $fieldvalue = '';
						switch ($fieldname) {
							case 'category_path':
								$query->clear();
								$query->select('path')->from('#__categories')->where('id = '.$record->catid);
								$db->setQuery($query);
								$fieldvalue = $db->loadResult();
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'article_url':
								// Let's create a SEF URL
								$article_url = $sef->getSEF(ContentHelperRoute::getArticleRoute($record->id, $record->catid));

								// Check for https, replace with http. https has unnecessary overhead
								if (substr($article_url, 0, 5) == 'https') $article_url = 'http'.substr($article_url, 5);
								$article_url = CsviHelper::replaceValue($field->replace, $article_url);
								$record->output[$column_id] = $article_url;
								break;
							case 'show_title':
							case 'link_titles':
							case 'show_intro':
							case 'show_category':
							case 'link_category':
							case 'show_parent_category':
							case 'link_parent_category':
							case 'show_author':
							case 'link_author':
							case 'show_create_date':
							case 'show_modify_date':
							case 'show_publish_date':
							case 'show_item_navigation':
							case 'show_icons':
							case 'show_print_icon':
							case 'show_email_icon':
							case 'show_vote':
							case 'show_hits':
							case 'show_noauth':
							case 'urls_position':
							case 'alternative_readmore':
							case 'article_layout':
							case 'show_publishing_options':
							case 'show_article_options':
							case 'show_urls_images_backend':
							case 'show_urls_images_frontend':
								$attributes = json_decode($record->attribs);
								$fieldvalue = $attributes->$fieldname;
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'meta_robots':
							case 'meta_author':
							case 'meta_rights':
							case 'meta_xreference':
								$metadata = json_decode($record->metadata);
								$newFieldname = str_ireplace('meta_', '', $fieldname);
								$fieldvalue = $metadata->$newFieldname;
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'custom':
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							default:
								// Check if we have any content otherwise use the default value
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$record->output[$column_id] = $fieldvalue;
								break;
						}
					}
					// Output the data
					$this->addExportFields($record);

					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') {
						$this->addExportContent($exportclass->NodeEnd());
					}

					// Output the contents
					$this->writeOutput();
				}
			}
			else {
				$this->addExportContent(JText::_('COM_CSVI_NO_DATA_FOUND'));
				// Output the contents
				$this->writeOutput();
			}
		}
	}
}