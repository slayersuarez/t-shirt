<?php
/**
 * Alpha user points import
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: couponimport.php 2273 2013-01-03 16:33:30Z RolandD $
 */

defined( '_JEXEC' ) or die;

class CsviModelUserpointimport extends CsviModelImportfile {

	// Private tables
	/** @var object contains the vm_coupons table */
	private $_userpoints = null;
	private $_userpoints_details = null;

	// Private variables

	// Public variables
	/** @var integer contains the userpoint ID */
	public $id = null;

	/**
	 * Constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.4
	 */
	public function __construct() {
		parent::__construct();
		// Load the tables that will contain the data
		$this->_loadTables();
		$this->loadSettings();
		
		// Set some initial values
		$this->date = JFactory::getDate();
	}

	/**
	 * Here starts the processing
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getStart() {
		// Load the data
		$this->loadData();

		// Get the logger
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);

		// Process data
		foreach ($this->csvi_data as $name => $fields) {
			foreach ($fields as $filefieldname => $details) {
				$value = $details['value'];
				// Check if the field needs extra treatment
				switch ($name) {
					default:
						$this->$name = $value;
						break;
				}
			}
		}

		// All good
		return true;
	}

	/**
	 * Process each record and store it in the database
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getProcessRecord() {
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);

		// Bind the data
		$this->_userpoints->bind($this);

		// Check the data
		if ($this->_userpoints->check()) {
			$this->_userpoints->load();
		}
		
		// Calculate the user points
		// Check what modification we need to do and apply it
		if (!is_null($this->points)) {
			$operation = substr($this->points, 0, 1);
			$value = substr($this->points, 1);
			$points = $this->_userpoints->points;
			switch ($operation) {
				case '+':
					$points += $value;
					break;
				case '-':
					$points -= $value;
					break;
				case '/':
					$points /= $value;
					break;
				case '*':
					$points *= $value;
					break;
				default:
					$points = $this->points;
					break;
			}
			$this->points = $points;
		}
		
		$this->_userpoints->bind($this);

		// Store the data
		if ($this->_userpoints->store()) {
			if ($this->queryResult() == 'UPDATE') $csvilog->AddStats('updated', JText::_('COM_CSVI_UPDATE_USERPOINTS'));
			else $csvilog->AddStats('added', JText::_('COM_CSVI_ADD_USERPOINTS'));
			
			// Store the debug message
			$csvilog->addDebug(JText::_('COM_CSVI_USERPOINTS_QUERY'), true);

			// Add details line
			$data = array();
			$data['referreid'] = $this->_userpoints->referreid;
			$data['points'] = $value;
			$data['insert_date'] = $this->date->toSql();
			$data['status'] = 1;
			$data['rule'] = 0;
			$data['approved'] = 1;
			$data['datareference'] = $csvilog->getFilename();
			$this->_userpoints_details->bind($data);
			if ($this->_userpoints_details->store()) {
				$csvilog->AddStats('added', JText::_('COM_CSVI_ADD_USERPOINTS_DETAILS'));
			}
			else $csvilog->AddStats('incorrect', JText::sprintf('COM_CSVI_USREPOINTS_DETAILS_NOT_ADDED', $this->_userpoints_details->getError()));
			
			// Store the debug message
			$csvilog->addDebug(JText::_('COM_CSVI_USERPOINTS_DETAILS_QUERY'), true);
			
		}
		else $csvilog->AddStats('incorrect', JText::sprintf('COM_CSVI_USREPOINTS_NOT_ADDED', $this->_userpoints->getError()));

		// Clean the tables
		$this->cleanTables();
	}

	/**
	 * Load the coupon related tables
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		3.0
	 */
	private function _loadTables() {
		$this->_userpoints = $this->getTable('userpoints');
		$this->_userpoints_details = $this->getTable('userpoints_details');
	}

	/**
	 * Cleaning the userpoints related tables
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return
	 * @since 		3.0
	 */
	protected function cleanTables() {
		$this->_userpoints->reset();
		$this->_userpoints_details->reset();

		// Clean local variables
		$class_vars = get_class_vars(get_class($this));
		foreach ($class_vars as $name => $value) {
			if (substr($name, 0, 1) != '_') {
				$this->$name = $value;
			}
		}
	}

	/**
	 * Get the Joomla user ID
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param		string	$name	The username to find the ID for
	 * @return		int	the user ID
	 * @since 		4.2
	 */
	private function _findUserId($name) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__users');
		$query->where('username = '.$db->quote($name));
		$db->setQuery($query);
		return $db->loadResult();
	}
}