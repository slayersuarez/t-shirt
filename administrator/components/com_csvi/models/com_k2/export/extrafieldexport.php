<?php
/**
 * K2 Extra field export class
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: customfieldsexport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

/**
 * Processor for K2 Extra field exports
 */
class CsviModelExtrafieldExport extends CsviModelExportfile {

	/**
	 * Start the export
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return 		void
	 * @since 		3.4
	 */
	public function getStart() {
		// Get some basic data
		$db = JFactory::getDbo();
		$csvidb = new CsviDb();
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$template = $jinput->get('template', null, null);
		$exportclass =  $jinput->get('export.class', null, null);
		$export_fields = $jinput->get('export.fields', array(), 'array');
		$helper = new Com_K2();

		// Build something fancy to only get the fieldnames the user wants
		$userfields = array();
		foreach ($export_fields as $column_id => $field) {
			switch ($field->field_name) {
				case 'option_name':
				case 'option_value':
				case 'option_target':
				case 'option_editor':
				case 'option_rows':
				case 'option_cols':
				case 'alias':
				case 'required':
				case 'showNull':
				case 'displayInFrontEnd':
					$userfields[] = $db->qn('value');
					break;
				case 'group_name':
					$userfields[] = $db->qn('group');
					break;
				case 'custom':
					break;
				default:
					$userfields[] = $db->qn($field->field_name);
					break;
			}
		}

		// Build the query
		$userfields = array_unique($userfields);
		$query = $db->getQuery(true);
		$query->select(implode(",\n", $userfields));
		$query->from($db->qn('#__k2_extra_fields', 'e'));

		$selectors = array();

		// Filter by published state
		$publish_state = $template->get('publish_state', 'general');
		if ($publish_state != '' && ($publish_state == 1 || $publish_state == 0)) {
			$selectors[] = $db->qn('e.published').' = '.$publish_state;
		}

		// Filter by group
		$groups = $template->get('extrafield_group', 'extrafield');
		if ($groups && $groups[0] != '*') {
			$selectors[] = $db->qn('group')." IN ('".implode("','", $groups)."')";
		}

		// Check if we need to attach any selectors to the query
		if (count($selectors) > 0 ) $query->where(implode("\n AND ", $selectors));

		// Ingore fields
		$ignore = array('custom', 'option_name', 'option_value', 'option_target', 'option_editor', 'option_rows', 'option_cols',
					'alias', 'required', 'showNull', 'displayInFrontEnd', 'group_name');

		// Check if we need to group the orders together
		$groupby = $template->get('groupby', 'general', false, 'bool');
		if ($groupby) {
			$filter = $this->getFilterBy('groupby', $ignore);
			if (!empty($filter)) $query->group($filter);
		}

		// Order by set field
		$orderby = $this->getFilterBy('sort', $ignore);
		if (!empty($orderby)) $query->order($orderby);

		// Add a limit if user wants us to
		$limits = $this->getExportLimit();

		// Execute the query
		$csvidb->setQuery($query, $limits['offset'], $limits['limit']);
		$csvilog->addDebug(JText::_('COM_CSVI_EXPORT_QUERY'), true);

		// There are no records, write SQL query to log
		if (!is_null($csvidb->getErrorMsg())) {
			$this->addExportContent(JText::sprintf('COM_CSVI_ERROR_RETRIEVING_DATA', $csvidb->getErrorMsg()));
			$this->writeOutput();
			$csvilog->AddStats('incorrect', $csvidb->getErrorMsg());
		}
		else {
			$logcount = $csvidb->getNumRows();
			$jinput->set('logcount', $logcount);
			if ($logcount > 0) {
				while ($record = $csvidb->getRow()) {
					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') $this->addExportContent($exportclass->NodeStart());
					foreach ($export_fields as $column_id => $field) {
						$fieldname = $field->field_name;
						$fieldreplace = $field->field_name.$field->column_header;
						// Add the replacement
						if (isset($record->$fieldname)) $fieldvalue = CsviHelper::replaceValue($field->replace, $record->$fieldname);
						else $fieldvalue = '';
						switch ($fieldname) {
							case 'option_name':
							case 'option_value':
							case 'option_target':
							case 'option_editor':
							case 'option_rows':
							case 'option_cols':
							case 'alias':
							case 'required':
							case 'showNull':
							case 'displayInFrontEnd':
								$extra = json_decode($record->value);
								$name = str_ireplace('option_', '', $fieldname);
								$fieldvalue = '';

								switch ($record->type)
								{
									case 'multipleSelect':
									case 'radio':
										if (in_array($fieldname, array('option_name', 'option_value')))
										{
											// Multiple selects are | separated
											$values = array();
											foreach ($extra as $extra_value)
											{
												if (isset($extra_value->$name))
												{
													$values[] = $extra_value->$name;
												}
											}
											$fieldvalue = implode('|', $values);
										}
										else
										{
											if (isset($extra[0]->$name))
											{
												$fieldvalue = $extra[0]->$name;
											}
										}
										break;
									case 'link':
										if (isset($extra[0]->$name))
										{
											$fieldvalue = $extra[0]->$name;
										}
										break;
									default:
										if (isset($extra[0]->$name))
										{
											$fieldvalue = $extra[0]->$name;
										}
										break;
								}
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'group_name':
								$query = $db->getQuery(true);
								$query->select('name');
								$query->from($db->qn('#__k2_extra_fields_groups'));
								$query->where($db->qn('id').' = '.$record->group);
								$db->setQuery($query);
								$fieldvalue = $db->loadResult();
								// Check if we have any content otherwise use the default value
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;

								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'custom':
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							default:
								// Check if we have any content otherwise use the default value
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$record->output[$column_id] = $fieldvalue;
								break;
						}
					}
					// Output the data
					$this->addExportFields($record);

					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') {
						$this->addExportContent($exportclass->NodeEnd());
					}

					// Output the contents
					$this->writeOutput();
				}
			}
			else {
				$this->addExportContent(JText::_('COM_CSVI_NO_DATA_FOUND'));
				// Output the contents
				$this->writeOutput();
			}
		}
	}
}