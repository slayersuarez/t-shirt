<?php
/**
 * K2 Item export class
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: customfieldsexport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

/**
 * Processor for Joomla Item exports
 */
class CsviModelItemExport extends CsviModelExportfile {

	private $_customfields_export = array();

	/**
	 * Start the export
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return 		void
	 * @since 		3.4
	 */
	public function getStart() {
		// Get some basic data
		$db = JFactory::getDbo();
		$csvidb = new CsviDb();
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$template = $jinput->get('template', null, null);
		$exportclass =  $jinput->get('export.class', null, null);
		$export_fields = $jinput->get('export.fields', array(), 'array');
		$this->_domainname = CsviHelper::getDomainName();
		$helper = new Com_K2();
		$sef = new CsviSef();
		$this->_loadExtraFields();

		// Build something fancy to only get the fieldnames the user wants
		$userfields = array();
		foreach ($export_fields as $column_id => $field) {
			switch ($field->field_name) {
				case 'category_path':
					$userfields[] = $db->qn('i').'.'.$db->qn('catid');
					break;
				case 'image':
				case 'image_url':
				case 'image_url_thumb':
					$userfields[] = "MD5(CONCAT('Image', ".$db->qn('id').")) AS ".$db->qn('image');
					break;
				case 'item_url':
					$userfields[] = $db->qn('i.id');
					break;
				case 'metadata_author':
				case 'metadata_robot':
					$userfields[] = $db->qn('i.metadata');
					break;
				case 'custom':
					break;
				default:
					if (!in_array($field->field_name, $this->_customfields_export)) $userfields[] = $db->qn($field->field_name);
					else $userfields[] = $db->qn('extra_fields');
					break;
			}
		}

		// Build the query
		$userfields = array_unique($userfields);
		$query = $db->getQuery(true);
		$query->select(implode(",\n", $userfields));
		$query->from($db->qn("#__k2_items", "i"));

		$selectors = array();

		// Filter by published state
		$publish_state = $template->get('publish_state', 'general');
		if ($publish_state != '' && ($publish_state == 1 || $publish_state == 0)) {
			$selectors[] = $db->qn('i.published').' = '.$publish_state;
		}

		// Filter by language
		$language = $template->get('item_language', 'general');
		if ($language != '*') {
			$selectors[] = $db->qn('i.language').' = '.$db->q($language);
		}

		// Filter by category
		$categories = $template->get('item_categories', 'item');
		if ($categories && $categories[0] != '') {
			$selectors[] = $db->qn('catid')." IN ('".implode("','", $categories)."')";
		}

		// Check if we need to attach any selectors to the query
		if (count($selectors) > 0 ) $query->where(implode("\n AND ", $selectors));

		// Ingore fields
		$ignore = array('custom', 'category_path', 'image', 'item_url', 'image_url', 'image_url_thumb', 'metadata_author', 'metadata_robot');
		$ignore = array_merge($ignore, $this->_customfields_export);

		// Check if we need to group the orders together
		$groupby = $template->get('groupby', 'general', false, 'bool');
		if ($groupby) {
			$filter = $this->getFilterBy('groupby', $ignore);
			if (!empty($filter)) $query->group($filter);
		}

		// Order by set field
		$orderby = $this->getFilterBy('sort', $ignore);
		if (!empty($orderby)) $query->order($orderby);

		// Add a limit if user wants us to
		$limits = $this->getExportLimit();

		// Execute the query
		$csvidb->setQuery($query, $limits['offset'], $limits['limit']);
		$csvilog->addDebug(JText::_('COM_CSVI_EXPORT_QUERY'), true);

		// There are no records, write SQL query to log
		if (!is_null($csvidb->getErrorMsg())) {
			$this->addExportContent(JText::sprintf('COM_CSVI_ERROR_RETRIEVING_DATA', $csvidb->getErrorMsg()));
			$this->writeOutput();
			$csvilog->AddStats('incorrect', $csvidb->getErrorMsg());
		}
		else {
			$logcount = $csvidb->getNumRows();
			$jinput->set('logcount', $logcount);
			if ($logcount > 0) {
				while ($record = $csvidb->getRow()) {
					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') $this->addExportContent($exportclass->NodeStart());
					foreach ($export_fields as $column_id => $field) {
						$fieldname = $field->field_name;
						$fieldreplace = $field->field_name.$field->column_header;
						// Add the replacement
						if (isset($record->$fieldname)) $fieldvalue = CsviHelper::replaceValue($field->replace, $record->$fieldname);
						else $fieldvalue = '';
						switch ($fieldname) {
							case 'category_path':
								$category_path = trim($helper->createCategoryPath($record->catid));
								if (strlen(trim($category_path)) == 0) $category_path = $field->default_value;
								$category_path = CsviHelper::replaceValue($field->replace, $category_path);
								$record->output[$column_id] = $category_path;
								break;
							case 'item_url':
								// Let's create a SEF URL
								$item_url = $sef->getSEF('index.php?option=com_k2&view=item&id='.$record->id);

								$item_url = CsviHelper::replaceValue($field->replace, $item_url);
								$record->output[$column_id] = $item_url;
								break;
							case 'image':
								// Check if there is already a product full image
								$image = $record->image.'.jpg';
								if (empty($image)) $image = $field->default_value;
								$image = CsviHelper::replaceValue($field->replace, $image);
								$record->output[$column_id] = $image;
								break;
							case 'image_url':
								// Check if there is already a product full image
								$picture_url = $this->_domainname.'/media/k2/items/cache/'.$record->image.'_'.$template->get('image_url', 'item', 'Generic').'.jpg';
								if (empty($picture_url)) $picture_url = $field->default_value;
								$picture_url = CsviHelper::replaceValue($field->replace, $picture_url);
								$record->output[$column_id] = $picture_url;
								break;
							case 'image_url_thumb':
								// Check if there is already a product full image
								$picture_url = $this->_domainname.'/media/k2/items/cache/'.$record->image.'_'.$template->get('image_url_thumb', 'item', 'S').'.jpg';
								if (empty($picture_url)) $picture_url = $field->default_value;
								$picture_url = CsviHelper::replaceValue($field->replace, $picture_url);
								$record->output[$column_id] = $picture_url;
								break;
							case 'metadata_author':
							case 'metadata_robot':
								$metadata = explode("\n", $record->metadata);

								foreach ($metadata as $key => $field)
								{
									list($name, $value) = explode('=', $field);
									switch ($fieldname)
									{
										case 'metadata_robot':
											if ($name == 'robots')
											{
												$fieldvalue = $value;
											}
											break;
										case 'metadata_author':
											if ($name == 'author')
											{
												$fieldvalue = $value;
											}
											break;
									}
								}

								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							case 'custom':
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
								$record->output[$column_id] = $fieldvalue;
								break;
							default:
								if (in_array($fieldname, $this->_customfields_export)) {
									// Get the extra fields values
									$record_extrafields = json_decode($record->extra_fields);
									if (is_array($record_extrafields))
									{
										$query = $db->getQuery(true);
										$query->select($db->qn(array('id', 'type', 'value')));
										$query->from('#__k2_extra_fields');
										$query->where('name = '.$db->quote($fieldname));
										$db->setQuery($query);
										$extra = $db->loadObject();
										$extra->value = json_decode($extra->value);

										if (is_object($extra))
										{
											// Check if this item has this field
											foreach ($record_extrafields as $key => $extrafield)
											{
												if ($extrafield->id == $extra->id)
												{
													switch ($extra->type)
													{
														case 'multipleSelect':
															// Multiple selects are | separated
															$values = array();
															foreach ($extra->value as $extra_value)
															{
																if (in_array($extra_value->value, $extrafield->value))
																{
																	$values[] = $extra_value->name;
																}
															}
															$fieldvalue = implode('|', $values);
															break;
														case 'radio':
															foreach ($extra->value as $extra_value)
															{
																if ($extra_value->value == $extrafield->value)
																{
																	$fieldvalue = $extra_value->name;
																}

															}
															break;
														case 'link':
															$fieldvalue = implode('|', $extrafield->value);
															break;
														default:
															$fieldvalue = $extrafield->value;
															break;
													}
												}
											}
											$fieldvalue = CsviHelper::replaceValue($field->replace, $fieldvalue);
										}
									}
								}
								// Check if we have any content otherwise use the default value
								if (strlen(trim($fieldvalue)) == 0) $fieldvalue = $field->default_value;
								$record->output[$column_id] = $fieldvalue;
								break;
						}
					}
					// Output the data
					$this->addExportFields($record);

					if ($template->get('export_file', 'general') == 'xml' || $template->get('export_file', 'general') == 'html') {
						$this->addExportContent($exportclass->NodeEnd());
					}

					// Output the contents
					$this->writeOutput();
				}
			}
			else {
				$this->addExportContent(JText::_('COM_CSVI_NO_DATA_FOUND'));
				// Output the contents
				$this->writeOutput();
			}
		}
	}

	/**
	 * Get a list of extra fields that can be used as available field
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		4.4.1
	 */
	private function _loadExtraFields() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("TRIM(name) AS title");
		$query->from($db->qn('#__k2_extra_fields'));
		$db->setQuery($query);
		$result = $db->loadColumn();
		if (!is_array($result)) $result = array();
		$this->_customfields_export = $result;
	}
}