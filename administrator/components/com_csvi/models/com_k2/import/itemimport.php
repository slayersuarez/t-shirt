<?php
/**
 * K2 item import
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: couponimport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

class CsviModelItemimport extends CsviModelImportfile {

	// Private tables
	private $_item = null;
	private $_categorymodel = null;


	// Public variables
	public $helper = null;
	public $id = null;
	public $alias = null;
	public $catid = null;
	public $category_path = null;
	public $image = null;

	/**
	 * Constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.4
	 */
	public function __construct() {
		parent::__construct();
		// Load the tables that will contain the data
		$this->_loadTables();
		$this->_loadCustomFields();
		$this->loadSettings();

		// Set some initial values
		$this->date = JFactory::getDate();
		$this->user = JFactory::getUser();
    }

	/**
	 * Here starts the processing
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getStart() {
		// Load the data
		$this->loadData();

		// Get the logger
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);

		// Load the helper
		$this->helper = new Com_K2();

		// Find the content id
		$this->id = $this->helper->getItemId();

		// Load the current content data
		$this->_item->load($this->id);

		// Process data
		foreach ($this->csvi_data as $name => $fields) {
			foreach ($fields as $filefieldname => $details) {
				$value = $details['value'];
				// Check if the field needs extra treatment
				switch ($name) {
					case 'published':
						switch (strtoupper($value)) {
							case 'Y':
								$value = 1;
								break;
							case 'N':
								$value = 0;
								break;
						}
						$this->$name = $value;
						break;
					default:
						$this->$name = $value;
						break;
				}
			}
		}

		// There must be an alias and catid or category_path
		if (empty($this->alias) && (empty($this->catid) && empty($this->category_path))) return false;

		// All good
		return true;
	}

	/**
	 * Process each record and store it in the database
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getProcessRecord() {
		$jinput = JFactory::getApplication()->input;
		$template = $jinput->get('template', null, null);
		$csvilog = $jinput->get('csvilog', null, null);

		if ($this->id && !$template->get('overwrite_existing_data', 'general')) {
			$csvilog->addDebug(JText::sprintf('COM_CSVI_DATA_EXISTS_ITEM', $this->alias));
			$csvilog->AddStats('skipped', JText::sprintf('COM_CSVI_DATA_EXISTS_ITEM', $this->alias));
		}
		else {
			// Check if there is a category ID
			if (empty($this->catid) && $this->category_path) {
				// Find the category ID
				$this->catid = $this->helper->getCategoryIdByPath($this->category_path);
			}

			// Check if we have a title
			if (empty($this->_item->title)) $this->_item->title = $this->alias;

			// Prepare metadata field
			if (!isset($this->metadata) && (isset($this->metadata_robot) || isset($this->metadata_author)))
			{
				// We create the field
				$values = array();
				$values[] = (isset($this->metadata_robot))? 'robots='.$this->metadata_robot : 'robots=';
				$values[] = (isset($this->metadata_author))? 'author='.$this->metadata_author : 'author=';

				$this->metadata = implode("\n", $values);
			}

			// Set the modified date as we are modifying the product
			if (!isset($this->modified))
			{
				$this->_item->modified = $this->date->toSql();
				$this->_item->modified_by = $this->user->id;
			}

			// Check if the field is a custom field used as an available field
			$this->_processCustomAvailableFields();

			// Set some default values
			if (empty($this->id))
			{
				// Set a creation date and time
				$this->_item->created = $this->date->toSql();
				$this->_item->created_by = $this->user->id;

				// Set the extra_fields value
				if (empty($this->_item->extra_fields))
				{
					$this->extra_fields = '[]';
				}

				// Set the param field
				if (!isset($this->params))
				{
					$this->params = '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}';
				}

				// Set the default metadata
				if (!isset($this->metadata))
				{
					$this->metadata = "robots=\r\nauthor=";
				}

				// Set the default language
				if (!isset($this->language))
				{
					$this->language = '*';
				}
			}

			// Bind the initial data
			$this->_item->bind($this);

			// Bind the data
			if ($this->_item->store()) {
				if ($this->queryResult() == 'UPDATE') $csvilog->AddStats('updated', JText::_('COM_CSVI_UPDATE_ITEM'));
				else $csvilog->AddStats('added', JText::_('COM_CSVI_ADD_ITEM'));

				// Store the debug message
				$csvilog->addDebug(JText::_('COM_CSVI_ITEM_QUERY'), true);

				// Add the image if needed
				if (!empty($this->image)) $this->_processMedia();
			}
			else {
				$csvilog->AddStats('incorrect', JText::sprintf('COM_CSVI_ITEM_NOT_ADDED', $this->_item->getError()));

				// Store the debug message
				$csvilog->addDebug(JText::_('COM_CSVI_ITEM_QUERY'), true);
				return false;
			}
		}

		// Clean the tables
		$this->cleanTables();
	}

	/**
	 * Load the coupon related tables
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		3.0
	 */
	private function _loadTables() {
		$this->_item = $this->getTable('item');
	}

	/**
	 * Cleaning the coupon related tables
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return
	 * @since 		3.0
	 */
	protected function cleanTables() {
		$this->_item->reset();

		// Clean local variables
		$class_vars = get_class_vars(get_class($this));
		foreach ($class_vars as $name => $value) {
			if (substr($name, 0, 1) != '_') {
				$this->$name = $value;
			}
		}
	}

	/**
	 * Process media files
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		4.0
	 */
	private function _processMedia() {
		$jinput = JFactory::getApplication()->input;
		$template = $jinput->get('template', null, null);
		$csvilog = $jinput->get('csvilog', null, null);
		// Check if any image handling needs to be done
		if ($template->get('process_image', 'image', false)) {

			// Load some helpers
			$this->config = new CsviCom_K2_Config();
			require_once (JPATH_ADMINISTRATOR.'/components/com_k2/lib/class.upload.php');

			// Set the maximum sizes
			$max_width = $template->get('resize_max_width', 'image', 1024);
			$max_height = $template->get('resize_max_height', 'image', 768);

			// Get the full path and name of the image
			$filename = md5('Image'.$this->_item->id);

			// Rename original image
			$image = JPATH_SITE.'/media/k2/items/src/'.$filename;

			// Check if the image already exists
			$imageExists = JFile::exists($image . '.jpg');
			if (!$imageExists || ($imageExists && $template->get('overwrite_image', 'image', false)))
			{
				JFile::move(JPATH_SITE.'/media/k2/items/src/'.trim($this->image), $image.'.jpg');
			}

			// Check if the thumbnail is not too big
			if (JFile::exists($image . '.jpg')) {
				$thumb_sizes = getimagesize($image.'.jpg');
				if ($thumb_sizes[0] < $max_width || $thumb_sizes[1] < $max_height) {
					$imagehelper = new Upload($image.'.jpg');

					// We need to create several thumbnails
					$sizes = array();
					$sizes['XS'] = $this->config->get('itemImageXS');
					$sizes['S'] = $this->config->get('itemImageS');
					$sizes['M'] = $this->config->get('itemImageM');
					$sizes['L'] = $this->config->get('itemImageL');
					$sizes['XL'] = $this->config->get('itemImageXL');
					$sizes['Generic'] = $this->config->get('itemImageGeneric');

					$savepath = JPATH_SITE.'/media/k2/items/cache';

					foreach ($sizes as $size => $width) {
						switch ($size) {
							default:
								$imagehelper->image_resize = true;
								$imagehelper->image_ratio_y = true;
								$imagehelper->image_convert = 'jpg';
								$imagehelper->jpeg_quality = $this->config->get('imagesQuality');
								$imagehelper->file_auto_rename = false;
								$imagehelper->file_overwrite = true;
								$imagehelper->file_new_name_body = $filename.'_'.$size;
								$imagehelper->image_x = $width;
								$imagehelper->Process($savepath);
								break;
						}
					}
				}
			}
			else {
				$csvilog->addDebug('Image '.$image.'.jpg not found');
			}
		}
	}

	/**
	 * Get a list of custom fields that can be used as available field
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		4.4.1
	 */
	private function _loadCustomFields() {
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('id').','.$db->qn('type').', TRIM('.$db->qn('name').') AS '.$db->qn('title'));
		$query->from($db->qn('#__k2_extra_fields'));
		$db->setQuery($query);
		$this->_customfields = $db->loadObjectlist();
		$csvilog->addDebug('Load custom fields', true);
	}

	/**
	 * Process custom fields that are used as available field
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		4.4.1
	 */
	private function _processCustomAvailableFields() {
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		$db = JFactory::getDbo();
		$order = 1;

		// Create the queries
		if (!empty($this->_customfields)) {
			// Require the K2 JSON class
			if (JFile::exists(JPATH_ADMINISTRATOR.'/components/com_k2/lib/JSON.php'))
			{
				require_once JPATH_ADMINISTRATOR.'/components/com_k2/lib/JSON.php';
				$json = new Services_JSON;

				foreach ($this->_customfields as $field) {
					$title = $field->title;
					$csvilog->addDebug('Processing custom available field: '.$title);
					if (isset($this->$title))
					{
						// Load the existing value
						$add = true;
						$extra_fields = $json->decode($this->_item->extra_fields);

						// Check if we need to do any formatting
						switch ($field->type) {
							case 'date':
								// Date format needs to be YYYY/MM/DD
								$value = $this->convertDate($this->$title, 'date');
								break;
							case 'multipleSelect':
								// We need to find the correct values
								$db = JFactory::getDbo();
								$query = $db->getQuery(true)
									->select($db->qn('value'))
									->from($db->qn('#__k2_extra_fields'))
									->where($db->qn('name').' = '.$db->q($field->title));
								$db->setQuery($query);
								$extra_values = $json->decode($db->loadResult());

								// Multiple selects are | separated
								$values = explode('|', $this->$title);
								$value = array();
								foreach ($extra_values as $extra_value)
								{
									if (in_array($extra_value->name, $values))
									{
										$value[] = $extra_value->value;
									}
								}

								break;
							case 'radio':
								// We need to find the correct values
								$db = JFactory::getDbo();
								$query = $db->getQuery(true)
								->select($db->qn('value'))
								->from($db->qn('#__k2_extra_fields'))
								->where($db->qn('name').' = '.$db->q($field->title));
								$db->setQuery($query);
								$extra_values = $json->decode($db->loadResult());

								// Multiple selects are | separated
								foreach ($extra_values as $extra_value)
								{
									if ($extra_value->name == $this->$title)
									{
										$value = (string) $extra_value->value;
									}
								}

								break;
							case 'link':
								if (strpos($this->$title, '|'))
								{
									$value = implode('|', $this->$title);
								}
								else
								{
									$value = array($this->$title, $this->$title, 'same');
								}
								break;
							case 'textfield':
							case 'labels':
							default:
								$value = $this->$title;
								break;
						}

						// Insert query if it is not empty
						if (!empty($value))
						{
							if (is_array($extra_fields))
							{
								foreach ($extra_fields as $ekey => $extra_field)
								{
									if ($field->id == $extra_field->id)
									{
										$extra_field->value = $value;
										$add = false;
									}
								}
							}

							if ($add)
							{
								$extra_field = new stdClass();
								$extra_field->id = $field->id;
								$extra_field->value = $value;
								$extra_fields[] = $extra_field;
							}

							// Store the new value
							$this->_item->extra_fields = $json->encode($extra_fields);
						}
					}
				}
			}
			else $csvilog->addDebug('K2 JSON Library not found');
		}
		else $csvilog->addDebug('No custom available fields found');
	}
}
