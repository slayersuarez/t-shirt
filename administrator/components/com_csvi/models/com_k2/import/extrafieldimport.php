<?php
/**
 * K2 Extra field import
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: couponimport.php 1924 2012-03-02 11:32:38Z RolandD $
 */

defined('_JEXEC') or die;

class CsviModelExtrafieldimport extends CsviModelImportfile {

	// Private tables
	private $_extrafield = null;

	// Public variables
	public $helper = null;
	public $id = null;
	public $name = null;

	/**
	 * Constructor
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.4
	 */
	public function __construct() {
		parent::__construct();
		// Load the tables that will contain the data
		$this->_loadTables();
		$this->loadSettings();
    }

	/**
	 * Here starts the processing
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getStart() {
		// Load the data
		$this->loadData();

		// Get the logger
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);

		// Load the helper
		$this->helper = new Com_K2();

		// Find the content id
		$this->id = $this->helper->getExtrafieldId();

		// Load the current content data
		$this->_extrafield->load($this->id);

		// Process data
		foreach ($this->csvi_data as $name => $fields) {
			foreach ($fields as $filefieldname => $details) {
				$value = $details['value'];
				// Check if the field needs extra treatment
				switch ($name) {
					case 'published':
						switch (strtoupper($value)) {
							case 'Y':
								$value = 1;
								break;
							case 'N':
								$value = 0;
								break;
						}
						$this->$name = $value;
						break;
					case 'group_name':
						$this->group = $this->_getGroupByName($value);
						break;
					default:
						$this->$name = $value;
						break;
				}
			}
		}

		// All good
		return true;
	}

	/**
	 * Process each record and store it in the database
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		public
	 * @param
	 * @return
	 * @since 		3.0
	 */
	public function getProcessRecord() {
		$jinput = JFactory::getApplication()->input;
		$template = $jinput->get('template', null, null);
		$csvilog = $jinput->get('csvilog', null, null);

		if ($this->id && !$template->get('overwrite_existing_data', 'general')) {
			$csvilog->addDebug(JText::sprintf('COM_CSVI_DATA_EXISTS_EXTRAFIELD', $this->name));
			$csvilog->AddStats('skipped', JText::sprintf('COM_CSVI_DATA_EXISTS_EXTRAFIELD', $this->name));
		}
		else {
			// Prepare the value field, this is an array
			if (!isset($this->value))
			{
				// We create the array
				$values = array();
				$value['name'] = (isset($this->option_name)) ? $this->option_name : '';
				$value['value'] = (isset($this->option_value)) ? $this->option_value: '';
				$value['target'] = (isset($this->option_target)) ? $this->option_target: '';
				$value['editor'] = (isset($this->option_editor)) ? $this->option_editor: '';
				$value['rows'] = (isset($this->option_rows)) ? $this->option_rows: '';
				$value['cols'] = (isset($this->option_cols)) ? $this->option_cols: '';
				$value['alias'] = (isset($this->alias)) ? $this->alias: '';
				$value['required'] = (isset($this->required)) ? $this->required : '';
				$value['showNull'] = (isset($this->showNull)) ? $this->showNull : '';
				$value['displayInFrontEnd'] = (isset($this->displayInFrontEnd)) ? $this->displayInFrontEnd : '';
				$values[] = $value;
				$this->value = json_encode($values);
			}
			
			// Bind the data
			if ($this->_extrafield->save($this)) {
				if ($this->queryResult() == 'UPDATE') $csvilog->AddStats('updated', JText::_('COM_CSVI_UPDATE_ITEM'));
				else $csvilog->AddStats('added', JText::_('COM_CSVI_ADD_ITEM'));

				// Store the debug message
				$csvilog->addDebug(JText::_('COM_CSVI_ITEM_QUERY'), true);
			}
			else {
				$csvilog->AddStats('incorrect', JText::sprintf('COM_CSVI_EXTRAFIELD_NOT_ADDED', $this->_extrafield->getError()));

				// Store the debug message
				$csvilog->addDebug(JText::_('COM_CSVI_EXTRAFIELD_QUERY'), true);
				return false;
			}
		}

		// Clean the tables
		$this->cleanTables();
	}

	/**
	 * Load the coupon related tables
	 *
	 * @copyright
	 * @author		RolandD
	 * @todo
	 * @see
	 * @access 		private
	 * @param
	 * @return
	 * @since 		3.0
	 */
	private function _loadTables() {
		$this->_extrafield = $this->getTable('extrafield');
	}

	/**
	 * Cleaning the coupon related tables
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return
	 * @since 		3.0
	 */
	protected function cleanTables() {
		$this->_extrafield->reset();

		// Clean local variables
		$class_vars = get_class_vars(get_class($this));
		foreach ($class_vars as $name => $value) {
			if (substr($name, 0, 1) != '_') {
				$this->$name = $value;
			}
		}
	}
	
	/**
	 * Find the group ID by name 
	 * 
	 * @copyright 
	 * @author 		RolandD
	 * @todo 
	 * @see 
	 * @access 		private
	 * @param 		string	$name	The name of the group
	 * @return 
	 * @since 		5.11
	 */
	private function _getGroupByName($name)
	{
		$jinput = JFactory::getApplication()->input;
		$csvilog = $jinput->get('csvilog', null, null);
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)->select($db->qn('id'))->from($db->qn('#__k2_extra_fields_groups'))->where($db->qn('name').' = '.$db->q($name));
		$db->setQuery($query);
		$group_id = $db->loadResult();

		$csvilog->addDebug('Find group ID', true);
		$csvilog->addDebug('Group ID found: '.$group_id);
		
		return $group_id;
	}
}