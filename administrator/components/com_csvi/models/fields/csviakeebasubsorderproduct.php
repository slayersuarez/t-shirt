<?php
/**
 * List the order products
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: csviakeebasubsorderproduct.php 2416 2013-04-25 19:04:54Z RolandD $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.form.helper');
JFormHelper::loadFieldClass('CsviForm');

/**
 * Select list form field with order products
 */
class JFormFieldCsviAkeebasubsOrderProduct extends JFormFieldCsviForm {

	protected $type = 'CsviAkeebasubsOrderProduct';

	/**
	 * Specify the options to load
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return 		array	an array of options
	 * @since 		4.0
	 */
	protected function getOptions() {
		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$template = $jinput->get('template', null, null);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$orderproduct = implode(',', $template->get('orderproduct', 'order', array(), 'array'));
		if (!empty($orderproduct)) {
			$query->select($db->qn('s.akeebasubs_level_id', 'value'));
			$query->select($db->qn('title', 'text'));
			$query->from($db->qn('#__akeebasubs_subscriptions', 's'));
			$query->leftJoin($db->qn('#__akeebasubs_levels', 'l').' ON '.$db->qn('s.akeebasubs_level_id').' = '.$db->qn('l.akeebasubs_level_id'));
			$query->where($db->qn('s.akeebasubs_level_id').' IN ('.$orderproduct.')');
			$query->order($db->qn('title'));
			$query->group($db->qn('s.akeebasubs_level_id'));
			$db->setQuery($query);
			$products = $db->loadObjectList();
			if (empty($products)) $products = array();
			return array_merge(parent::getOptions(), $products);
		}
		else return parent::getOptions();
	}
}