<?php
/**
 * List the K2 Extra field groups
 *
 * @author 		Roland Dalmulder
 * @link 		http://www.csvimproved.com
 * @copyright 	Copyright (C) 2006 - 2014 RolandD Cyber Produksi. All rights reserved.
 * @license 	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @version 	$Id: csviakeebasubsorderuser.php 1924 2012-03-02 11:32:38Z RolandD $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.form.helper');
JFormHelper::loadFieldClass('CsviForm');

/**
 * Select list form field with order users
 */
class JFormFieldCsviK2Groups extends JFormFieldCsviForm 
{

	protected $type = 'CsviK2Groups';

	/**
	 * Specify the options to load
	 *
	 * @copyright
	 * @author 		RolandD
	 * @todo
	 * @see
	 * @access 		protected
	 * @param
	 * @return 		array	an array of options
	 * @since 		4.0
	 */
	protected function getOptions() 
	{
		$this->options = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)->select($db->qn('id', 'value').','.$db->qn('name', 'text'))->from($db->qn('#__k2_extra_fields_groups'));
		$db->setQuery($query);
		$this->options = $db->loadObjectList();
		
		return array_merge(parent::getOptions(), $this->options);
	}
}
