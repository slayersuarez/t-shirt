<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Julee' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Coda:400,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
<script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
		
				function valida_envia(){
					
					if(document.fvalida.nombre.value.length==0){
						alert("Por Favor escriba su nombre");
						document.fvalida.nombre.focus();
						return false;
					}
					
					if(document.fvalida.email.value.indexOf('@')==-1){
					alert("La direccion de email no es correcta");
					document.fvalida.email.focus();
					return false;
					}
					
					if(document.fvalida.telefono.value.length==0){
						alert("Por Favor escriba su telefono");
						document.fvalida.telefono.focus();
						return false;
					}

					if(document.fvalida.file.value.length==0){
						alert("Debe adjuntar un archivo");
						document.fvalida.file.focus();
						return false;
					}

					if(document.fvalida.mensaje.value.length==0){
						alert("Por Favor escriba su mensaje");
						document.fvalida.mensaje.focus();
						return false;
					}

					document.fvalida.submit();
					
					
				}
</script>

<style>

body{
	background-image: url('../images/pop-up.jpg');
}

ul{
	width: 100%;
	padding: 0;
}

li{
	width: 100%;
	display: block;
	float: left;
	text-align: left;
	margin-bottom: 6px;
}

p{
	font-family: 'Open Sans', sans-serif;
	font-size: 14px;
	text-align: justify;
	color: #5A5A5A;
	line-height: 18px;
}
h2{
	font-family: 'Open Sans', sans-serif;
	font-size: 28px;
	color: #5A5A5A;
	border-bottom: 1px dashed;
	width: 700px;
	text-align: left;
	padding-bottom: 10px;
	margin-bottom: 10px;
	font-weight: bold;
}


label{
	color: #4E4E4E;
	font-family: 'Open Sans', sans-serif;
	font-size: 14px;
	color: #5A5A5A;
	width: 100%;
	display: inline-block;
	float: left;
	text-align: left;
	margin-right: 20px;
	}

input[type="text"] {
	padding: 3px 0px;
	float: left;
	margin: 0;
	box-shadow: 0px 10px 20px -17px inset;
	-ms-box-shadow: 0px 10px 20px -17px inset;
	width: 480px;
}

select{
	width: 150px;
}

input[type="submit"] {
	text-decoration: none;
	color: #FFF;
	font-family: 'Open Sans';
	font-size: 14px;
	background: rgb(119,119,119); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(119,119,119,1) 0%, rgba(224,224,224,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(119,119,119,1)), color-stop(100%,rgba(224,224,224,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(119,119,119,1) 0%,rgba(224,224,224,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(119,119,119,1) 0%,rgba(224,224,224,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(119,119,119,1) 0%,rgba(224,224,224,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(119,119,119,1) 0%,rgba(224,224,224,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#777777', endColorstr='#e0e0e0',GradientType=0 ); /* IE6-9 */
	text-transform: uppercase;
	border: 0;
	padding: 5px 12px;
	position: relative;
	border-radius: 5px;
	display: inline-block;
	width: 100px;
	text-align: center;
	float: right;
}

textarea {
	width: 660px;
	padding: 5px 0px;
	border-radius: 5px;
	border: 2px solid #203961;
	background-color: #F0F0F0;
	min-height: 100px;

}



.icono-mail img{
	float:left;
	margin-right: 10px;
	vertical-align: middle;
}

.icono-mail h4{
	font-weight: lighter;
	color: #59B047;
}

</style>

<form id="commentForm"  name="fvalida" method="post" action="procesar.php" onSubmit="return valida_envia()" enctype="multipart/form-data">

<ul>
<h2>Empleo</h2>
<p>Si estas interesado en hacer parte del equipo de T-Shirt Lab por favor envianos los siguientes datos</p>
<li><label>Nombre Completo</label><input type="text" name="nombre" /></li>
<li><label>Correo Electrónico</label><input type="text" name="email" /></li>
<li><label>Celular</label><input type="text" name="celular" /></li>
<li><label>Área de interés</label></li>
<li>
	<select name="area">
		<option>Diseño</option>
		<option>Ventas</option>
		<option>Administración</option>
	</select>
</li>
<li><label>Por favor incluye tu Hoja de vida (Solo archivos Word o PDF)</label></li>
<li><input type="file" name="file" id="file"/></li>
</ul>

<div class="actions">
	<input type="submit" value="Enviar">	
</div>
</form>




