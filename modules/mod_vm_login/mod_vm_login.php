<?php
/*------------------------------------------------------------------------
* Module VM Login
* author    Netbase Team
* copyright Copyright (C) 2012 www.cms-extensions.net All Rights Reserved.
* @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* Websites: www.cms-extensions.net
* Technical Support:  Forum - www.cms-extensions.net
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// count instances
if (!isset($GLOBALS['yoo_logins'])) {
	$GLOBALS['yoo_logins'] = 1;
} else {
	$GLOBALS['yoo_logins']++;
}

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

$params->def('greeting', 1);

$type 	= modYOOloginHelper::getType();
$return	= modYOOloginHelper::getReturnURL($params, $type);

$user =& JFactory::getUser();

// init vars
$style                 = $params->get('style', 'niftyquick');
$pretext               = $params->get('pretext', '');
$posttext              = $params->get('posttext', '');
$text_mode             = $params->get('text_mode', 'input');
$login_button          = $params->get('login_button', 'icon');
$logout_button         = $params->get('logout_button', 'text');
$auto_remember         = $params->get('auto_remember', '1');
$lost_password         = $params->get('lost_password', '1');
$lost_username         = $params->get('lost_username', '1');
$registration          = $params->get('registration', '1');
$style_login           = $params->get('style_login', 'joomla_style');
$cart_vms              = $params->get('cart_vms', '1');


// css parameters
$yoologin_id           = $GLOBALS['yoo_logins'];

$module_base           = JURI::base() . 'modules/mod_vm_login/';

switch ($style) {
	case "niftydefault":
		require(JModuleHelper::getLayoutPath('mod_vm_login', 'niftydefault'));
   		break;
	case "niftyquick":
		require(JModuleHelper::getLayoutPath('mod_vm_login', 'niftyquick'));
   		break;
	case "quick":
		require(JModuleHelper::getLayoutPath('mod_vm_login', 'quick'));
   		break;
	default:
		require(JModuleHelper::getLayoutPath('mod_vm_login', 'default'));
}

$document =& JFactory::getDocument();
$document->addStyleSheet($module_base . 'mod_vm_login.css.php');