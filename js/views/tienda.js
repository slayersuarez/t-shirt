/**
* Tienda View
* version : 1.0
* package: tshirtlab.frontend
* package: tshirtlab.frontend.mvc
* author: Jhonny Gil
* Creation date: November 2014
*
* Manage all the frontend events in Tshirt Lab Store
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var TiendaView = {};

	// Extends my object from Backbone events
	TiendaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #ver-items': 'despliega',
				'click .info_tallas': 'infoTallas',
				'click .ic-cerrar': 'mostrarMagic',
				'keyup .tallas-valores': 'calcularPrecio'
			}

		,	view: this

		,	quantity: 0

		,	initialize: function(){
				_.bindAll(
					this,
					'calcularPrecio'
				);
			}


		,	despliega: function( e ){

				e.preventDefault();

				$( ".despliegue" ).hide( "slow" );
				$( ".sub" ).show( "slow" );

				// if( $( ".sub" ).is( ':visible' ) )
				// 	$( ".sub" ).hide( "slow" );

				// if( ! $( ".sub" ).is( ':visible' ) )
				// 	$( ".sub" ).show( "slow" );
				// 	$( ".despliegue" ).hide( "slow" );

			}

		,	calcularPrecio: function( e ){

				var suma = 0;
				var total = 0;
				var target = e.currentTarget;
				var unitPrice = $( '#unitario' ).val();

				$.each( $( '.tallas-valores' ), function( key, value ){

					var q = parseInt( $( value ).val() );

					if ( isNaN(q))
						q = 0;

					if( typeof q == 'number' )
						suma = suma + q;

				});

				if (!isNaN(suma)){

					$( '#total_camisetas' ).val( suma );
					$( '#result_suma' ).val( '$' + unitPrice * suma );
				}
	      	}


		,	infoTallas: function( e ){

				e.preventDefault();

				if( $( ".infotallas" ).is( ':visible' ) )
					return;

				if( ! $( ".infotallas" ).is( ':visible' ) )
					$( ".infotallas" ).show( "slow" );
					$( ".magic" ).hide( "slow" );


			}	      	

		,	mostrarMagic: function( e ){

				e.preventDefault();

				if( $( ".magic" ).is( ':visible' ) )
					$( ".magic" ).hide( "slow" );
					$( ".infotallas" ).hide( "slow" );


				if( ! $( ".magic" ).is( ':visible' ) )
					$( ".magic" ).show( "slow" );
					$( ".infotallas" ).hide( "slow" );


			}				
			
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.TiendaView = new TiendaView();

})( jQuery, this, this.document, this.Misc, undefined );