/**
* Tienda View
* version : 1.0
* package: tshirtlab.frontend
* package: tshirtlab.frontend.mvc
* author: Jhonny Gil
* Creation date: November 2014
*
* Manage all the frontend events in Tshirt Lab Store
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var TiendaView = {};

	// Extends my object from Backbone events
	TiendaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup #cantidad_mugs': 'calcularPrecioMugs'
			}

		,	view: this

		,	quantity: 0

		,	initialize: function(){
				_.bindAll(
					this,
					'calcularPrecioMugs'
				);
			}


		,	calcularPrecioMugs: function( e ){

				var target = e.currentTarget;
				var unitPriceMugs = $( '#unitario_mugs' ).val();
				var cantidadMugs = $( '#cantidad_mugs' ).val();

					$( '#total_mugs' ).val( '$' + unitPriceMugs * cantidadMugs );
				
	      	}
	      					
			
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.TiendaView = new TiendaView();

})( jQuery, this, this.document, this.Misc, undefined );