jQuery.noConflict();

jQuery(document).ready(function($){
	//Iphone radio-btn style - Setup html structure
	$(".radio.btn-group").each(function(){
		var i = 0,
			total_els = $(this).find("input:radio").length;
		$(this).find("input:radio").each(function(){
			var self = $(this),
				label = self.next();
			if(self.is(':checked')){
				label.addClass("selected");
			}
			//Detect enable/disable by value
			radio_value = self.val().toLowerCase();
			if(radio_value=='0' || radio_value=='false' || radio_value=='desc') {
				label.addClass("cb-disable");
			} else {
				label.addClass("cb-enable");
			}
			
			if (i==0) {
				label.addClass("cb-first");
			}
			if (i==total_els-1) {
				label.addClass("cb-last");
			}
			var label_text = "<span>"+label.html()+"</span>";
			label.html(label_text);
			self.hide();
			i++;
		});
	});
	//Click
	$("label").click(function(){
		var parent = $(this).parents('.radio.btn-group');
		//Remove all selected class
		$('label',parent).removeClass('selected');
		//Add selected class for clicked option
		$(this).addClass('selected');
	});
});